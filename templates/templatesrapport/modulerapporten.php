<?php
// Page initialisation
$location = "jsrapport/modulerapporten";
?>
<!DOCTYPE html>
<html lang="nl" id="htmldoc">
    <head>

        <style>
            #top-col {
                padding-bottom: 15px;
            }           
            .form-name {
                display: inline-block !important;
                margin-right: 10px;
                width: 300px !important;
            }           
            .form-desc {
                display: inline-block !important;
                margin-right: 10px;
                width: 520px !important;
            }
            .validationSummary
            {
                color: red;
            }
            .hide{
                display: none;
            }
            #addCompetenceContainer {
                margin-top: 50px;
            }
            #addCompetenceForm {
                width: 600px;
            }
            #addCompetence {
                display: inline-block;
            }
            #checkbox {
                display: inline-block;
                margin-left: 25px;
            }
            *[id^='editbtn-']{
                margin-right: 10px;
            }
        </style>

        <?php include_once('templates/hddepends.php') ?>
    </head>

    <body>
        <?php include_once('templates/menu.php') ?>
        
        <!-- Header container -->
        <div class="page-header container" id="headContainer">
            <h1 id="projectHeader" data-value="<?php echo $courseid ?>"><?php echo $coursename ?></h1>
          </div>
        <!-- Validation Summary -->
        <div class="validationSummary hide">
            <h2>Vak werd niet opgeslaan!</h2>
            <ul></ul>
        </div>
        
        <div class='container' data-bind="foreach: evaluatieElementen">
            <div class="panel-group" id="level1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#level1" data-bind="attr:{'href': '#collapse' + id()}, text: number() + ' ' + name() + '  ---  ' + description()"></a>
                            <a class="pull-right"><span class="glyphicon glyphicon-trash glyphicon-btn" data-bind="attr:{'id': 'removebtn-' + id()}"></span></a>
                            <span class="glyphicon glyphicon-pencil glyphicon-btn pull-right" data-bind="attr:{'id': 'editbtn-' + id()}"></span>
                        </h4>
                    </div>
                    <div data-bind="attr:{'id': 'collapse' + id()}" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="panel-group" id="level2" data-bind="foreach: children">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#level2" data-bind="attr:{'href': '#collapse' + id()}, text: number() + ' ' + name() + '  ---  ' + description()"></a>
                                            <a class="pull-right"><span class="glyphicon glyphicon-trash glyphicon-btn" data-bind="attr:{'id': 'removebtn-' + id()}"></span></a>
                                            <span class="glyphicon glyphicon-pencil glyphicon-btn pull-right" data-bind="attr:{'id': 'editbtn-' + id()}"></span>
                                        </h4>
                                    </div>
                                    <div data-bind="attr:{'id': 'collapse' + id()}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="panel-group" id="level3" data-bind="foreach: children">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#level3" data-bind="attr:{'href': '#collapse' + id()}, text: number() + ' ' + name() + '  ---  ' + description()"></a>
                                                            <a class="pull-right"><span class="glyphicon glyphicon-trash glyphicon-btn" data-bind="attr:{'id': 'removebtn-' + id()}"></span></a>
                                                            <span class="glyphicon glyphicon-pencil glyphicon-btn pull-right" data-bind="attr:{'id': 'editbtn-' + id()}"></span>
                                                        </h4>
                                                    </div>
                                                    <div data-bind="attr:{'id': 'collapse' + id()}" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="panel-group" id="level4" data-bind="foreach: children">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#level4" data-bind="attr:{'href': '#collapse' + id()}, text: number() + ' ' + name() + '  ---  ' + description()"></a>
                                                                            <a class="pull-right"><span class="glyphicon glyphicon-trash glyphicon-btn" data-bind="attr:{'id': 'removebtn-' + id()}"></span></a>
                                                                            <span class="glyphicon glyphicon-pencil glyphicon-btn pull-right" data-bind="attr:{'id': 'editbtn-' + id()}"></span>
                                                                        </h4>
                                                                    </div>
                                                                    <div data-bind="foreach: children, attr:{'id': 'collapse' + id()}" class="panel-collapse collapse">
                                                                        <div class="panel-body">
                                                                            <span data-bind="text: number() + ' ' + name() + '  ---  ' + description()"></span>
                                                                            <a class="pull-right"><span class="glyphicon glyphicon-trash glyphicon-btn" data-bind="attr:{'id': 'removebtn-' + id()}"></span></a>
                                                                            <span class="glyphicon glyphicon-pencil glyphicon-btn pull-right" data-bind="attr:{'id': 'editbtn-' + id()}"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row">
                <div id="bottom-col" class="col-md-6">
                    <button class="btn btn-lg addCompetenceBtn">Competentie toevoegen</button>                  
                </div>
            </div>
        </div>
        
        <div class="container hide" id="addCompetenceContainer">
            <form id="addCompetenceForm" class="pull-left">
                <div class="form-group">
                    <input type="text" class="form-control input-lg" placeholder="naam" name="name">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control input-lg" placeholder="beschrijving" name="description">
                </div>
                <div class="form-group">
                    <input id="parentComplete" class="form-control input-lg" placeholder="bovenliggend item" name="parent">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg" id="addCompetence">Voeg toe</button>
                    <div class="checkbox" id="checkbox">
                        <label>
                            <input id="checkSubitem" type="checkbox"> Subitem
                        </label>
                    </div>
                </div>
            </form>
        </div>
        
        <!-- Feedback Form -->
        
        <div class="container feedbackForm">
            <form id="feedbackForm">
                <textarea id="feedback" name="feedback" placeholder="feedback over deze pagina" rows="7" cols="60"></textarea>
                <input type="submit" value="Indienen" style="display: block" />
            </form>
        </div>
        
        <!-- Feedback Form -->

        <?php include_once('templates/jsdepends.php') ?>
        <script>
            var courseid = <?php echo $courseid ?>
        </script>
        <script src="/js/jsrapport/feedback.js"></script>
    </body>
</html>
