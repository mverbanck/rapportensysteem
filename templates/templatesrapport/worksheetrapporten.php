<?php
    $location = "jsrapport/worksheetrapporten";
?>

<!DOCTYPE html>
<html lang="nl" id="htmldoc">
    <head>
        <?php include_once('templates/hddepends.php');  ?>

        
        <style>
            input#studentComplete {
                width: 300px;
                height: 35px;
                margin-bottom: 20px;
                display: inline-block;
            }
        </style>
        <link rel="stylesheet" href="../css/cssrapport/blue/style.css" type="text/css" media="print, projection, screen" />
    </head>
    
    <body>
        <?php include_once('templates/menu.php') ?>

        <!-- Header container -->
        <div class="container">
            <h1 class="page-header">Werkfiches</h1>
        </div>

        <!-- Content container -->
        <div class="container">
            <div class="big-info">Selecteer vak:</div>
            <div class="row">
                <div class="dropdown col-md-4">
                    <button class="btn btn-wide btn-default btn-courseRapport dropdown-toggle" type="button" id="courseRapport" data-toggle="dropdown" aria-expanded="true">
                        <span class="text-center">Vak</span>
                        <span class="pull-right caret-down caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-courseRapport ul-wide" role="menu" aria-labelledby="courseRapport" data-bind="foreach: availableCourses" id="testcliker">
                        <li class="li-wide" role="presentation"><a role="menuitem" tabindex="-1" href="#" data-bind="attr:{'id': 'coursebtn-' + id}"><span data-bind="text: name"></span></a> </li>
                       </ul>
                </div><p id="errormessage" class="text-danger"></p>
            </div>
        </div>
        
        <!-- Content container -->
        <div class="container">
            <table class="table table-striped tablesorter" id="testSort" >
                <thead>
                <tr>
                    <th>Werkfiche ID</th>
                    <th>Naam</th>
                    <th>Acties</th>
                </tr>
                </thead>
                <tbody data-bind="foreach: tabledata">
                <tr>
                    <td data-bind="text: tid">--</td>     
                    <td data-bind="text: tname">--</td>     
                    <td>
                        <span class="glyphicon glyphicon-pencil glyphicon-btn" data-bind="attr:{'id': 'editbtn-' + tid}" data-toggle="tooltip"  data-placement="top" title="Wijzig Worksheet"></span>
                        <span class="glyphicon glyphicon-check glyphicon-btn" data-bind="attr:{'id': 'assessbtn-' + tid}" data-toggle="tooltip" data-placement="top" title="Beoordeel student"></span>
                        <span class="glyphicon glyphicon-copyright-mark glyphicon-btn" data-bind="attr:{'id': 'copybtn-' + tid}"data-toggle="tooltip" data-placement="top" title="Copy"></span>
                        <span class="glyphicon glyphicon-trash glyphicon-btn" data-bind="attr:{'id': 'removebtn-' + tid}" data-toggle="tooltip"  data-placement="top" title="Delete"></span>
                    </td>     
                </tr>
                </tbody>
            </table>
        </div>
        
        <div class="container">
            <!-- Pagination -->
            <ul class="pagination float_left">
                <li id="pager-prev-btn"><a href="#" >&laquo;</a></li>
                <li class="pager-nr-btn"><a href="#" >1</a></li>
                <li class="pager-nr-btn"><a href="#" >2</a></li>
                <li class="pager-nr-btn"><a href="#">3</a></li>
                <li class="pager-nr-btn"><a href="#">4</a></li>
                <li class="pager-nr-btn"><a href="#">5</a></li>
                <li id="pager-next-btn"><a href="#" >&raquo;</a></li>
            </ul>

            <button type="button" class="btn btn-default pagination-button" id="addWorksheetBtn">
                <span class="glyphicon glyphicon-plus"></span> Maak nieuw
            </button>
            
            <div id="assessStudentForm" class="container">
                <div class="row">
                    <label>Naam student:</label> <input id="studentComplete" class="form-control input-lg" name="studentName" /><br>
                    <button id="assessStudentBtn" class="btn btn-default">Beoordeel</button>
                </div>
            </div>
        </div>
        
        <!-- Feedback Form -->
        
        <div class="container feedbackForm">
            <form id="feedbackForm">
                <textarea id="feedback" name="feedback" placeholder="feedback over deze pagina" rows="7" cols="60"></textarea>
                <input type="submit" value="Indienen" style="display: block" />
            </form>
        </div>
        
        <!-- Feedback Form -->

        <?php include_once('templates/jsdepends.php') ?>
        <script src="/js/jsrapport/feedback.js"></script>
        <script src="/js/jsrapport/jquery.tablesorter.js"></script>
    </body>
</html>