<?php
    $location = "jsrapport/assessrapporten";
?>

<!DOCTYPE html>
<html lang="nl" id="htmldoc">
    <head>
        <?php include_once('templates/hddepends.php') ?>
        <link rel="stylesheet" href="../css/cssrapport/blue/style.css" type="text/css" media="print, projection, screen" />
        <style>
            td {
                text-align: center;
            }
        </style>
    </head>
    
    <body>
        <?php include_once('templates/menu.php') ?>
        
        <!-- Header container -->
        <div class="container">
            <h1 class="page-header">Overzicht werkfiches studenten</h1>
        </div>

        <!-- Content container -->
        <div class="container " id="menu">

                <div id="nameinput" class="form-inline col-lg-4 ">
                   <label>Naam student:</label>
                    <input id="studentsComplete" name="studentname" class="coursesInputField form-control " />
                </div>
                <div class="dropdown pull-right col-lg-4" id="dropdown" >

                    <button class="btn btn-wide col-lg-4  btn-default btn-courseRapport dropdown-toggle" type="button" id="courseRapport" data-toggle="dropdown" aria-expanded="true">
                        <span class="text-center">Vakken</span>
                        <span class="pull-right caret-down caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-courseRapport ul-wide " role="menu" aria-labelledby="courseRapport" data-bind="foreach: availableCourses" id="testcliker">
                        <li class="li-wide" role="presentation"><a role="menuitem" tabindex="-1" href="#" data-bind="attr:{'id': 'coursebtn-' + id}"><span data-bind="text: name"></span></a> </li>
                    </ul>
                </div>
            </div>

        <!-- Content container, originele table -->
        <div class="container">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Naam</th>
                    <th>Datum</th>
                    <th>Resultaat</th>
                    <th>Acties</th>

                </tr>
                </thead>
                <tbody data-bind="foreach: tabledata">
                <tr>
                    <td data-bind="text: twork">--</td>
                    <td data-bind="text: tname">--</td>
                    <td data-bind="text: tdatum">--</td>
                    <td data-bind="text: tscore">--</td>
                    <td>
                        <a  data-bind="attr:{'href': '/api/worksheetscores/' + id + '/' + tname + '/' + viewModel.currentStudentName + '/' + viewModel.currentCourseName + '/' + viewModel.cleanDate + '/' + tscore}" data-toggle="tooltip" data-placement="top" title="Details"><span class="glyphicon glyphicon-list-alt" ></span></a>
                        <span class="glyphicon glyphicon-trash glyphicon-btn" data-bind="attr:{'id': 'removebtn-' + id}" data-toggle="tooltip" title="Delete"></span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        
        <!-- Pagination and action container -->
        <div class="container">
            <!-- Pagination -->
            <ul class="pagination float_left">
                <li id="pager-prev-btn"><a href="#" >&laquo;</a></li>
                <li class="pager-nr-btn"><a href="#" >1</a></li>
                <li class="pager-nr-btn"><a href="#" >2</a></li>
                <li class="pager-nr-btn"><a href="#">3</a></li>
                <li class="pager-nr-btn"><a href="#">4</a></li>
                <li class="pager-nr-btn"><a href="#">5</a></li>
                <li id="pager-next-btn"><a href="#" >&raquo;</a></li>
            </ul>
        </div>

        <!-- Content container, originele table -->
        <div class="container">
            <table class="table table-striped" id="ilttable">
                <thead>
                <tr data-bind="foreach: tabledata">
                    <th></th>
                    <th data-bind="text: id + ' ' + tname">--</th>
                </tr>
                </thead>
                <tbody data-bind="foreach: evaluatieElementen">
                    <tr data-bind="attr:{'id': id()}">
                        <th data-bind="text:number() + ' ' + name() + ': ' + description()"></th>
                    </tr>
                </tbody>
            </table>
        </div>

        <!-- Feedback Form -->
        <div class="container feedbackForm">
            <form id="feedbackForm">
                <textarea id="feedback" name="feedback" placeholder="feedback over deze pagina" rows="7" cols="60"></textarea>
                <input type="submit" value="Indienen" style="display: block" />
            </form>
        </div>
        <!-- Feedback Form -->

        <?php include_once('templates/jsdepends.php') ?>
        <script src="/js/jsrapport/feedback.js"></script>

        <!-- TODO table sorter weer uit commentaar hallen zodra tables af zijn. -->
        <!-- <script src="/js/jsrapport/jquery.tablesorter.js"></script> -->
    </body>
</html>
