<?php
    // Page initialisation
    $location = "jsrapport/studentmanagementrapporten";
?>
<!DOCTYPE html>
<html lang="nl" id="htmldoc">
    <head>
        <?php include_once('templates/hddepends.php') ?>

        
        <style>
            div#filter label {
                margin-left: 20px;
            }
            input[type=checkbox] {
                margin-right: 5px;
            }
            input#studentsComplete {
                display:inline-block;
                width: 300px;
                height: 35px;
            }
            div#menu {
                margin-bottom: 20px;
            }
            div#nameinput {
                width: 500px;
            }

        </style>
        <link rel="stylesheet" href="../css/cssrapport/blue/style.css" type="text/css" media="print, projection, screen" />
    </head>

    <body>
        <?php include_once('templates/menu.php') ?>

        <!-- Header container -->
        <div class="container">
            <h1 class="page-header">Studentenbeheer</h1>
        </div>

        <!-- Content container -->
        <div class="container" id="menu">
            <div class="dropdown col-md-4" id="nameinput">
                <label>Naam student:</label> 
                <input id="studentsComplete" name="studentname" class="coursesInputField form-control input-lg" />
            </div>
            <div id="filter" class="pull-right">Filter:
                <label><input type="checkbox" name="volgt" id="volgt" checked />Volgt</label>
                <label><input type="checkbox" name="volgtNiet" id="volgtNiet" checked />Volgt Niet </label>
                <label><input type="checkbox" name="geslaagd" id="geslaagd" checked />Geslaagd </label>
            </div>
        </div>

        <!-- Content container -->
        <div class="container">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Naam</th>
                        <th>Status</th>
                        <th>Acties</th>
                    </tr>
                </thead>
                <tbody data-bind="foreach: tabledata">
                    <tr class="tableRow ">
                        <td data-bind="text: tcode">--</td>
                        <td data-bind="text: tname">--</td>
                        <td class="rowStatus" data-bind="text: tstat">--</td>
                        <td>
                            <span class="glyphicon glyphicon-minus-sign glyphicon-btn" data-bind="attr:{'id': 'delbtn-' + tid}"data-toggle="tooltip"  data-placement="top" title="Schrijf uit"></span>
                            <span class="glyphicon glyphicon-plus-sign glyphicon-btn" data-bind="attr:{'id': 'addbtn-' + tid}"data-toggle="tooltip"  data-placement="top" title="Schrijf in"></span>
                            <span class="glyphicon glyphicon-ok-circle glyphicon-btn" data-bind="attr:{'id': 'sucbtn-' + tid}"data-toggle="tooltip"  data-placement="top" title="Geslaagd"></span>
                            <span class="glyphicon glyphicon-repeat glyphicon-btn" data-bind="attr:{'id': 'resbtn-' + tid}"data-toggle="tooltip"  data-placement="top" title="Reset"></span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <!-- Pagination and action container -->
        <div class="container">
            <!-- Pagination -->
            <ul class="pagination float_left">
                <li id="pager-prev-btn"><a href="#" >&laquo;</a></li>
                <li class="pager-nr-btn"><a href="#" >1</a></li>
                <li class="pager-nr-btn"><a href="#" >2</a></li>
                <li class="pager-nr-btn"><a href="#">3</a></li>
                <li class="pager-nr-btn"><a href="#">4</a></li>
                <li class="pager-nr-btn"><a href="#">5</a></li>
                <li id="pager-next-btn"><a href="#" >&raquo;</a></li>
            </ul>
        </div>
        <!-- Feedback Form -->

        <div class="container feedbackForm">
            <form id="feedbackForm">
                <textarea id="feedback" name="feedback" placeholder="feedback over deze pagina" rows="7" cols="60"></textarea>
                <input type="submit" value="Indienen" style="display: block" />
            </form>
        </div>
        <?php include_once('templates/jsdepends.php') ?>
        <script src="/js/jsrapport/feedback.js"></script>
        <script src="/js/jsrapport/jquery.tablesorter.js"></script>
    </body>
</html>