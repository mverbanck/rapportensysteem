<?php
    $location = "jsrapport/courseteachersrapporten";
?>

<!DOCTYPE html>
<html lang="nl" id="htmldoc">
<head>
    <?php include_once('templates/hddepends.php') ?>
    <link rel="stylesheet" href="/css/cssrapport/blue/style.css" type="text/css" media="print, projection, screen" />
</head>

<body>
<?php include_once('templates/menu.php') ?>

    <!-- Header container -->
    <div class="container">
        <h1 class="page-header" id="projectHeader" data-value="<?php echo $courseteachersid ?>"><?php echo $courseteachersname ?></h1>
    </div>

    <!-- Content container -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Leerkracht</th>
                        <th>Acties</th>
                    </tr>

                    </thead>
                    <tbody data-bind="foreach: tabledata">
                    <tr>
                        <td data-bind="text: tteacher">--</td>
                        <td>
                            <span class="glyphicon glyphicon-trash glyphicon-btn" data-bind="attr:{'id': 'removebtn-' + tid}"data-toggle="tooltip" title="Verwijder Leerkracht"></span>
                        </td>
                    </tr>
                    </tbody>
                    </table>
            </div>

            <!-- Pagination and action container -->
            <div class="container">
                <!-- Pagination -->
                <ul class="pagination float_left">
                    <li id="pager-prev-btn"><a href="#" >&laquo;</a></li>
                    <li class="pager-nr-btn"><a href="#" >1</a></li>
                    <li class="pager-nr-btn"><a href="#" >2</a></li>
                    <li class="pager-nr-btn"><a href="#">3</a></li>
                    <li class="pager-nr-btn"><a href="#">4</a></li>
                    <li class="pager-nr-btn"><a href="#">5</a></li>
                    <li id="pager-next-btn"><a href="#" >&raquo;</a></li>
                </ul>

                <button type="button" class="btn btn-default pagination-button" id="addCourseTeachers">
                    <span class="glyphicon glyphicon-plus" ></span> Voeg toe
                </button>
            </div>
            
            <div class="container">
                <button type="button" class="btn btn-default" id="goBackBtn">
                    <span class="glyphicon glyphicon-arrow-left"></span> Terug
                </button>
            </div>
        </div>
    </div>
    
    <!-- Feedback Form -->
        
        <div class="container feedbackForm">
            <form id="feedbackForm">
                <textarea id="feedback" name="feedback" placeholder="feedback over deze pagina" rows="7" cols="60"></textarea>
                <input type="submit" value="Indienen" style="display: block" />
            </form>
        </div>
        
        <!-- Feedback Form -->

    <?php include_once('templates/jsdepends.php') ?>
<script src="/js/jsrapport/feedback.js"></script>
<script src="/js/jsrapport/jquery.tablesorter.js"></script>
</body>
</html>