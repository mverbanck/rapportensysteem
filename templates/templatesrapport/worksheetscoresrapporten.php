<?php
    $location = "jsrapport/worksheetscoresrapporten";
?>

<!DOCTYPE html>
<html lang="nl" id="htmldoc">
    <head>
        <?php include_once('templates/hddepends.php'); ?>
        
        <style>
            #scoretable {
                margin-top: 30px;
            }
            #level-1 td {
                padding-left: 20px;
            }
            #level-2 td {
                padding-left: 40px;
            }
            #level-3 td {
                padding-left: 60px;
            }
            #level-4 td {
                padding-left: 80px;
            }
            #level-5 td {
                padding-left: 100px;
            }
        </style>
    </head>
    
    <body>
        <?php include_once('templates/menu.php') ?>

        <!-- Header container -->
        <div class="container">
            <h1 id="header" class="page-header"><?php echo $studentname ?> - <?php echo $sheetname ?></h1>
        </div>
        
        <!-- Content container -->
        <div class="container">
            <label>Vak:</label> <span><?php echo $course ?></span><br>
            <label>Datum:</label> <span><?php echo $date ?></span><br>
            <label>Resultaat:</label> <span><?php echo $result ?></span><br>
        </div>
        
        <div class="container" id="scoretable">
            <table class="table table-striped">
                <tbody data-bind="foreach: scores">
                    <tr data-bind="attr:{'id': 'level-' + level}">
                        <td data-bind="text: name">--</td>
                        <td data-bind="text: description">--</td>
                        <td data-bind="text: score">--</td>
                    </tr>
                </tbody>
            </table>
            
            <button type="button" class="btn btn-default" id="goBackBtn">
                <span class="glyphicon glyphicon-arrow-left"></span> Terug
            </button>
        </div>
        
        <!-- Feedback Form -->
        
        <div class="container feedbackForm">
            <form id="feedbackForm">
                <textarea id="feedback" name="feedback" placeholder="feedback over deze pagina" rows="7" cols="60"></textarea>
                <input type="submit" value="Indienen" style="display: block" />
            </form>
        </div>
        
        <!-- Feedback Form -->

        <?php include_once('templates/jsdepends.php') ?>
        <script src="/js/jsrapport/feedback.js"></script>
        <script>
            var sheetid     = <?php echo $sheetid ?>;
            var studentname = "<?php echo $studentname ?>";
            var sheetname   = "<?php echo $sheetname ?>";
            var course      = "<?php echo $course ?>";
            var date        = "<?php echo $date ?>";
            var result      = "<?php echo $result ?>";
        </script>
    </body>
</html>