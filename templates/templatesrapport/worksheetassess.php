<?php
    $location = "jsrapport/worksheetassess";
?>

<!DOCTYPE html>
<html lang="nl" id="htmldoc">
    <head>

        <style>
            #top-col {
                padding-bottom: 15px;
            }          
            .form-name {
                display: inline-block !important;
                margin-right: 10px;
                width: 750px !important;
            }
            .validationSummary
            {
                color: red;
            }
            .hide{
                display: none;
            }         
            form {
                margin-bottom: 100px;
            }
            #list ul {
                margin: 0;
                padding: 0;
                list-style-type: none;
            }
            #date {
                width: 200px;
                text-align: center;
            }
            #label {
                display:inline-block;
                margin-right: 20px;
                margin-top: 30px;
                margin-bottom: 30px;
            }
            #submit {
                display: block;
            }
            #assessdrop {
                margin-top: -8px;
            }
        </style>
        
        <?php include_once('templates/hddepends.php');  ?>
    </head>
    
    <body>
        <?php include_once('templates/menu.php') ?>

        <!-- Header container -->
        <p id="storage" data-bind="attr:{'data-value': <?php echo $courseid ?>}" style="display: none"><?php echo $userid ?></p>
        <p id="storage2" data-bind="attr:{'data-value': <?php echo $worksheetfromstudid ?>}" style="display: none"></p>
        <div class="container">
            <h1 id="header" class="page-header" data-bind="attr:{'data-value': <?php echo $workid ?>}"><?php echo "$workname - $username" ?></h1>
        </div>
        
        <div class="container">
            <p class="text-danger" id="errormessage"></p>
        </div>
        
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form id="worksheetassess" >      <!-- action="/assessrapporten" -->
                        <div class="form-group">
                            <label>Datum</label><br />
                            <input type="text" id="date" class="form-control input-sm" />
                        </div>
                        <br />
                        
                        <!-- list van modules, competenties en criteria -->
                        <div class="formgroup">
                            <label>Competenties</label>
                            <div id="list" data-bind="foreach: evaluatieElementen">
                                <div class="panel-group" id="level1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <div class="dropdown col-md-2 pull-left" id="assessdrop">
                                                    <button class="btn btn-wide btn-default btn-assessScore dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                                                        <span class="text-center" data-bind="attr:{'id': 'score-' + id()}">Geen</span>
                                                        <span class="pull-right caret-down caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-assessMethod ul-wide" role="menu" aria-labelledby="assessScore" data-bind="attr:{'id': 'assessScore-' + id()}, foreach: viewModel.assessMethod" name="assessScore">
                                                        <li class="li-wide" role="presentation"><a role="menuitem" tabindex="-1" href="#" data-bind="text: score"></a></li>
                                                    </ul>
                                                </div>
                                                <a data-toggle="collapse" data-parent="#level1" data-bind="attr:{'href': '#collapse' + id()}, text: number() + ' ' + name() + '  ---  ' + description()"></a>
                                                <!-- Original dropdown location -->
                                            </h4>
                                        </div>
                                        <div data-bind="attr:{'id': 'collapse' + id()}" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="panel-group" id="level2" data-bind="foreach: children">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <div class="dropdown col-md-2 pull-left" id="assessdrop">
                                                                    <button class="btn btn-wide btn-default btn-assessScore dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                                                                        <span class="text-center" data-bind="attr:{'id': 'score-' + id()}">Geen</span>
                                                                        <span class="pull-right caret-down caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu dropdown-assessMethod ul-wide" role="menu" aria-labelledby="assessScore" data-bind="attr:{'id': 'assessScore-' + id()}, foreach: viewModel.assessMethod" name="assessScore">
                                                                        <li class="li-wide" role="presentation"><a role="menuitem" tabindex="-1" href="#" data-bind="text: score"></a></li>
                                                                    </ul>
                                                                </div>
                                                                <a data-toggle="collapse" data-parent="#level2" data-bind="attr:{'href': '#collapse' + id()}, text: number() + ' ' + name() + '  ---  ' + description()"></a>
                                                                <!-- Original dropdown location -->
                                                            </h4>
                                                        </div>
                                                        <div data-bind="attr:{'id': 'collapse' + id()}" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <div class="panel-group" id="level3" data-bind="foreach: children">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <div class="dropdown col-md-2 pull-left" id="assessdrop">
                                                                                    <button class="btn btn-wide btn-default btn-assessScore dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                                                                                        <span class="text-center" data-bind="attr:{'id': 'score-' + id()}">Geen</span>
                                                                                        <span class="pull-right caret-down caret"></span>
                                                                                    </button>
                                                                                    <ul class="dropdown-menu dropdown-assessMethod ul-wide" role="menu" aria-labelledby="assessScore" data-bind="attr:{'id': 'assessScore-' + id()}, foreach: viewModel.assessMethod" name="assessScore">
                                                                                        <li class="li-wide" role="presentation"><a role="menuitem" tabindex="-1" href="#" data-bind="text: score"></a></li>
                                                                                    </ul>
                                                                                </div>
                                                                                <a data-toggle="collapse" data-parent="#level3" data-bind="attr:{'href': '#collapse' + id()}, text: number() + ' ' + name() + '  ---  ' + description()"></a>
                                                                                <!-- Original dropdown location -->
                                                                            </h4>
                                                                        </div>
                                                                        <div data-bind="attr:{'id': 'collapse' + id()}" class="panel-collapse collapse">
                                                                            <div class="panel-body">
                                                                                <div class="panel-group" id="level4" data-bind="foreach: children">
                                                                                    <div class="panel panel-default">
                                                                                        <div class="panel-heading">
                                                                                            <h4 class="panel-title">
                                                                                                <div class="dropdown col-md-2 pull-left" id="assessdrop">
                                                                                                    <button class="btn btn-wide btn-default btn-assessScore dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                                                                                                        <span class="text-center" data-bind="attr:{'id': 'score-' + id()}">Geen</span>
                                                                                                        <span class="pull-right caret-down caret"></span>
                                                                                                    </button>
                                                                                                    <ul class="dropdown-menu dropdown-assessMethod ul-wide" role="menu" aria-labelledby="assessScore" data-bind="attr:{'id': 'assessScore-' + id()}, foreach: viewModel.assessMethod" name="assessScore">
                                                                                                        <li class="li-wide" role="presentation"><a role="menuitem" tabindex="-1" href="#" data-bind="text: score"></a></li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <a data-toggle="collapse" data-parent="#level4" data-bind="attr:{'href': '#collapse' + id()}, text: number() + ' ' + name() + '  ---  ' + description()"></a>
                                                                                                <!-- Original dropdown location -->
                                                                                            </h4>
                                                                                        </div>
                                                                                        <div data-bind="foreach: children, attr:{'id': 'collapse' + id()}" class="panel-collapse collapse">
                                                                                            <div class="panel-body">
                                                                                                <div class="dropdown col-md-2 pull-left" id="assessdrop">
                                                                                                    <button class="btn btn-wide btn-default btn-assessScore dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                                                                                                        <span class="text-center" data-bind="attr:{'id': 'score-' + id()}">Geen</span>
                                                                                                        <span class="pull-right caret-down caret"></span>
                                                                                                    </button>
                                                                                                    <ul class="dropdown-menu dropdown-assessMethod ul-wide" role="menu" aria-labelledby="assessScore" data-bind="attr:{'id': 'assessScore-' + id()}, foreach: viewModel.assessMethod" name="assessScore">
                                                                                                        <li class="li-wide" role="presentation"><a role="menuitem" tabindex="-1" href="#" data-bind="text: score"></a></li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <span data-bind="text: number() + ' ' + name() + '  ---  ' + description()"></span>
                                                                                                <!-- Original dropdown location -->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        
                        <p id="label">Geslaagd voor werkfiche</p>
                        <input id="worksheetPassed" type="checkbox" name="passed" />
                        
                        <input id="submit" value="Opslaan" class="btn btn-primary submitBtn" />    <!-- type="submit" -->
                    </form>
                </div>
            </div>
        </div>
        
        <!-- Feedback Form -->
        
        <div class="container feedbackForm">
            <form id="feedbackForm">
                <textarea id="feedback" name="feedback" placeholder="feedback over deze pagina" rows="7" cols="60"></textarea>
                <input type="submit" value="Indienen" style="display: block" />
            </form>
        </div>
        
        <!-- Feedback Form -->

        <?php include_once('templates/jsdepends.php') ?>
        <script src="/js/jsrapport/feedback.js"></script>
    </body>
</html>