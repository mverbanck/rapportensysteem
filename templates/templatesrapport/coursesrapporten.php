<?php
    // Page initialisation
    $location = "jsrapport/coursesrapporten";
?>
<!DOCTYPE html>
<html lang="nl" id="htmldoc">
    <head>
        <?php include_once('templates/hddepends.php') ?>
        <link rel="stylesheet" href="../css/cssrapport/blue/style.css" type="text/css" media="print, projection, screen" />

    </head>

    <body>
        <?php include_once('templates/menu.php') ?>

        <!-- Header container -->
        <div class="container">
            <h1 class="page-header">Vakken</h1>
        </div>

        <!-- Content container -->
        <div class="container">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Naam</th>
                        <th>Beschrijving</th>
                        <th>Acties</th>
                    </tr>
                </thead>
                <tbody data-bind="foreach: tabledata">
                    <tr>
                        <td data-bind="text: tcode">--</td>
                        <td data-bind="text: tname">--</td>
                        <td data-bind="text: tdesc">--</td>
                        <td>
                            <span class="glyphicon glyphicon-pencil glyphicon-btn" data-bind="attr:{'id': 'editbtn-' + tid}" data-toggle="tooltip" title="edit"></span>
                            <a data-bind="attr:{'href': '/api/coursemodule/' + tid + '/' + tname}"><span class="glyphicon glyphicon-plus glyphicon-btn" data-bind="attr:{'id': 'managebtn-' + tid}" data-toggle="tooltip" title="edit evaluatie-elementen"></span></a>
                            <a data-bind="attr:{'href': '/api/courseteachers/' + tid + '/' + tname}"><span class="glyphicon glyphicon-user glyphicon-btn" data-bind="attr:{'id': 'studentbtn-' + tid}"data-toggle="tooltip" title="wijzig leerkrachten"></span></a>
                            <span class="glyphicon glyphicon-trash glyphicon-btn" data-bind="attr:{'id': 'removebtn-' + tid}" data-toggle="tooltip" title="Delete"></span>

                        </td>
                    </tr>
                </tbody>
            </table>

        </div>

        <!-- Pagination and action container -->
        <div class="container">
            <!-- Pagination -->
            <ul class="pagination float_left">
                <li id="pager-prev-btn"><a href="#" >&laquo;</a></li>
                <li class="pager-nr-btn"><a href="#" >1</a></li>
                <li class="pager-nr-btn"><a href="#" >2</a></li>
                <li class="pager-nr-btn"><a href="#">3</a></li>
                <li class="pager-nr-btn"><a href="#">4</a></li>
                <li class="pager-nr-btn"><a href="#">5</a></li>
                <li id="pager-next-btn"><a href="#" >&raquo;</a></li>
            </ul>

            <button type="button" class="btn btn-default pagination-button" id="addCourseBtn">
                <span class="glyphicon glyphicon-plus"></span> Maak nieuw
            </button>
        </div>
        
        <!-- Feedback Form -->
        
        <div class="container feedbackForm">
            <form id="feedbackForm">
                <textarea id="feedback" name="feedback" placeholder="feedback over deze pagina" rows="7" cols="60"></textarea>
                <input type="submit" value="Indienen" style="display: block" />
            </form>
        </div>
        
        <!-- Feedback Form -->

        <?php include_once('templates/jsdepends.php') ?>
        <script src="/js/jsrapport/feedback.js"></script>
        <script src="/js/jsrapport/jquery.tablesorter.js"></script>
    </body>
</html>