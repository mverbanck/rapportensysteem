<?php
$location = "jsrapport/worksheetpropertiesrapporten";
?>

<!DOCTYPE html>
<html lang="nl" id="htmldoc">
<head>
    <?php include_once('templates/hddepends.php');  ?>

    <style>
        #top-col {
            padding-bottom: 15px;
        }
        .form-name {
            display: inline-block !important;
            margin-right: 10px;
            width: 300px !important;
        }
        .form-desc {
            display: inline-block !important;
            margin-right: 10px;
            width: 520px !important;
        }
        .validationSummary
        {
            color: red;
        }
        .hide{
            display: none;
        }
        #addCompetenceContainer {
            margin-top: 50px;
        }
        #addCompetence {
            display: inline-block;
        }
        #checkbox {
            display: inline-block;
            margin-left: 25px;
        }
    </style>
    <link href="/css/cssrapport/report.css" rel="stylesheet">
</head>

<body>
<?php include_once('templates/menu.php') ?>

<!-- Header container -->
<p id="storage" data-bind="attr:{'data-value': <?php echo $courseid ?>}" style="display: none"><?php echo $edit ?></p>   <!-- veldje om courseid in op te slaan -->
<div class="container">
    <h1 id="header" class="page-header" data-bind="attr:{'data-value': <?php echo $sheetid ?>}"><?php echo $sheetname ?></h1>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <p id="errormessage" class="text-danger"></p>
            <form id="worksheetform" action="/worksheetrapporten">
                <div class="form-group">
                    <label>Materiaal</label>
                    <textarea class="form-control" rows="3" name="equipment" id="equipment" form="worksheetform"></textarea>
                </div>
                <br />
                <div class="form-group">
                    <label>Werkmethode</label>
                    <textarea class="form-control" rows="3" name="method" id="method" form="worksheetform"></textarea>
                </div>
                <br />


                <!--Weergeven competenties-->
                <label>Competenties</label>
                <br />
                <div class='container' data-bind="foreach: evaluatieElementen">
                    <div class="panel-group" id="level1">
                        <div class="panel panel-default">
                            <div class="panel-heading">                        <!-- collapseThree: zorgt voor het openklappen van het eerste niveau! -->
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#level1" data-bind="attr:{'href': '#collapse' + id()}, text: number() + ' ' + name() + '  ---  ' + description()"></a>
                                    <a class="pull-right"><input type="checkbox" data-bind="attr:{'id': 'checkbox-' + id()}"></span></span></a>
                                </h4>
                            </div>
                            <div data-bind="attr:{'id': 'collapse' + id()}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="panel-group" id="level2" data-bind="foreach: children">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#level2" data-bind="attr:{'href': '#collapse' + id()}, text: number() + ' ' + name() + '  ---  ' + description()"></a>
                                                    <a class="pull-right"><input type="checkbox" data-bind="attr:{'id': 'checkbox-' + id()}"></span></span></a>
                                                </h4>
                                            </div>
                                            <div data-bind="attr:{'id': 'collapse' + id()}" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="panel-group" id="level3" data-bind="foreach: children">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#level3" data-bind="attr:{'href': '#collapse' + id()}, text: number() + ' ' + name() + '  ---  ' + description()"></a>
                                                                    <a class="pull-right"><input type="checkbox" data-bind="attr:{'id': 'checkbox-' + id()}"></span></span></a>
                                                                </h4>
                                                            </div>
                                                            <div data-bind="attr:{'id': 'collapse' + id()}" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div class="panel-group" id="level4" data-bind="foreach: children">
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-heading">
                                                                                <h4 class="panel-title">
                                                                                    <a data-toggle="collapse" data-parent="#level4" data-bind="attr:{'href': '#collapse' + id()}, text: number() + ' ' + name() + '  ---  ' + description()"></a>
                                                                                    <a class="pull-right"><input type="checkbox" data-bind="attr:{'id': 'checkbox-' + id()}"></span></span></a>
                                                                                </h4>
                                                                            </div>
                                                                            <div data-bind="foreach: children, attr:{'id': 'collapse' + id()}" class="panel-collapse collapse">
                                                                                <div class="panel-body">
                                                                                    <span data-bind="text: name() + '  ---  ' + description()"></span>
                                                                                    <a class="pull-right"><input type="checkbox" data-bind="attr:{'id': 'checkbox-' + id()}"></span></span></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Einde competenties-->

                <label>Evaluatie-methode</label>
                <div class="row">
                    <div class="dropdown col-md-2">
                        <button class="btn btn-wide btn-default btn-assessMethod dropdown-toggle" type="button" id="assessMethod" data-toggle="dropdown" aria-expanded="true">
                            <span class="text-center">Geen</span>
                            <span class="pull-right caret-down caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-assessMethod ul-wide" role="menu" aria-labelledby="assessMethod" name="assessMethod">
                            <li class="li-wide" role="presentation"><a role="menuitem" tabindex="-1" href="#">A - D</a></li>
                            <li class="li-wide" role="presentation"><a role="menuitem" tabindex="-1" href="#">1 - 10</a></li>
                            <li class="li-wide" role="presentation"><a role="menuitem" tabindex="-1" href="#"><span>Aangepast</span></a></li>
                        </ul>
                    </div>
                </div>
                <input type="button" id="submittemp" value="Later wijzigen" class="btn btn-primary" />
                <input type="button" id="submitdef" value="Definitief opslaan" class="btn btn-primary" />
                <input type="reset" id="cancel" value="Leegmaken" class="btn btn-default" />
            </form>
        </div>
    </div>
</div>

<!-- Feedback Form -->
        
<div class="container feedbackForm">
    <form id="feedbackForm">
        <textarea id="feedback" name="feedback" placeholder="feedback over deze pagina" rows="7" cols="60"></textarea>
        <input type="submit" value="Indienen" style="display: block" />
    </form>
</div>

<!-- Feedback Form -->

<?php include_once('templates/jsdepends.php') ?>
<script src="/js/jsrapport/feedback.js"></script>
</body>
</html>