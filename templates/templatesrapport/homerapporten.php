<?php
    $location = "jsrapport/homerapporten";
?>

<!DOCTYPE html>
<html lang="nl" id="htmldoc">
    <head>
        <?php include_once('templates/hddepends.php');

        //Connection with local database: include_once('database.php') + Db::getConnection()  ?>

    </head>
    
    <body>
        <?php include_once('templates/menu.php') ?>

        <!-- Header container -->
        <div class="container">
            <h1 class="page-header">Rapportensysteem</h1>
        </div>
        
        <!-- Content container -->
        <div class="container">
            <div class="starter-template">
                <ul>
                    <li>Rapporten: Zien resultaten van een bepaalde student per vak.<br />
                        Zowel per werkfiche als ILT.</li>
                    <li>Vakken: Vakken beheren, leerkracht toekennen of wissen en evaluatie-elementen aanpassen.</li>
                    <li>Werkfiches: Nieuwe werkfiches toevoegen, wissen & wijzigen.<br />
                    Of studenten quoteren.</li>
                    <li>Studentenbeheer: Leerling toevoegen en wissen als volger van een bepaald vak. <br />
                    Of de status geslaagd meegeven.</li>
                    <li>Profielbeheer: Klikken op naam rechtsboven -> Account.</li>
                    <li>Account/ Rechtenbeheer: Klikken op naam rechtsboven -> Admin Panel.</li>
                </ul>
            </div>
        </div>
        
        <!-- Feedback Form -->
        
        <div class="container feedbackForm">
            <form id="feedbackForm">
                <textarea id="feedback" name="feedback" placeholder="feedback over deze pagina" rows="7" cols="60"></textarea>
                <input type="submit" value="Indienen" style="display: block" />
            </form>
        </div>
        
        <!-- Feedback Form -->

        <?php include_once('templates/jsdepends.php') ?>
        <script src="/js/jsrapport/feedback.js"></script>
    </body>
</html>