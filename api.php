<?php

/*
 * Grader API functions 
 */

// Load required files
require_once('dptcms/database.php');


// Database class for connection handling
class GraderAPI {
    /*
     * Get a page from currently stored projecttypes. 
     * @start: the item start with
     * @count: the number of items on the page 
     */

    public static function getAllDataFromProject($id) {
        return ClassDAO::getAllDataFromProject($id);
    }

    public static function removeUser($userid)
    {
        return UserDAO::removeUser($userid);
    }

    public static function createStudentAndCoupleToListId($listid, $mail, $firstname, $lastname) {
        $studentid = ClassDao::insertStudent($mail, $firstname, $lastname);
        $listid2 = ClassDao::insertStudentlist_Student($studentid, $listid);

    }

    public static function updateProfilePicture($pictureid) {
        $userid = Security::getLoggedInId();
        return UserDAO::updateUserProfilePicture($userid, $pictureid);
    }

    public static function getLoggedInId() {
        return Security::getLoggedInId();

    }

    /*
     * Get user information given a username
     */
    public static function getUserData($username) {
        return UserDAO::getUserByUsername($username, true);
    }

    /*
     * Get user information given a id
     */
    public static function getEditUserDataById($uid) {
        return UserDAO::getEditUserById($uid);
    }

    /*
     * Get all users
     */
    public static function getAllUsersData() {
        return UserDAO::getAllUsers();
    }

    /*
     * Get all users with roles
     */
    public static function getAllUsersWithRolesData() {
        return UserDAO::getAllUsersWithRoles();
    }


    /*
     * Update user status
     */
    public static function updateUserStatus($uid, $status) {
        return UserDAO::updateUserStatus($uid, $status);
    }

    /*
     * Update user
     */
    public static function updateUser($id, $firstname, $lastname, $username, $status) {
        return UserDAO::updateUser($id, $firstname, $lastname, $username, $status);
    }

    /*
     * remove user roles
     */
    public static function removeUserRoles($id) {
        return UserDAO::removeUserRoles($id);
    }

    /*
     * Add user role
     */
    public static function addUserRole($id, $role) {
        return UserDAO::addUserRole($id, $role);
    }

}
