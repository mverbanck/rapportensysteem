<?php
/*
 * Grader API functions for rapporten 
 */
// Load required files
require_once('dptcms/databaserapporten.php');
Class RapportAPI {
    public static function getAllCourses($start, $count) {
        return rapportenDAO::getAllCourses($start, $count);
    }

    public static function getCoursesOverviewStudent($start, $count, $userid) {
        return rapportenDAO::getCoursesOverviewStudent($start, $count, $userid);
    }

    public static function getAllEvaluatieElementsFromCourse($courseid) {
        return rapportenDAO::getAllEvaluatieElementsFromCourse($courseid);
    }

    public static function getAllCourse() {
        return rapportenDAO::getAllCourse();
    }

    public static function getCoursesFromTeacher($userid) {
        return rapportenDAO::getCoursesFromTeacher($userid);
    }
    public static function getCoursesFromStudent($userid) {
        return rapportenDAO::getCoursesFromStudent($userid);
    }

    public static function getILTscore($courseid, $userid) {
        return rapportenDAO::getILTscore($courseid, $userid);
    }
    
    public static function getAllStudents() {
        return rapportenDAO::getAllStudents();
    }

    public static function getcriteriaBydoelstelling($id) {
        return rapportenDAO::getcriteriasBydoelstelling($id);
    }
    
    public static function addWorksheetToCourse($name, $courseid) {
        $id = rapportenDAO::addWorksheetToCourse($name, $courseid);
        if ($id != null) {
            return array(
                "id" => $id,
                "name" => $name,
                "course" => $courseid);
        } else {
            return -1;
        }
    }

    public static function CopyWorksheet($name, $courseid) {
        $id = rapportenDAO::addWorksheetToCourse($name, $courseid);
        //getAllEvaluatieElementsFromWorksheet($id);
        if ($id != null) {
            return array(
                "id" => $id,
                "name" => $name,
                "course" => $courseid);
        } else {
            return -1;
        }
    }
    
    public static function getAllWorksheets($courseid, $start, $count) {
        return rapportenDAO::getAllWorksheets($courseid, $start, $count);
    }

    public static function getAllWorksheetsNoPager($courseid) {
        return rapportenDAO::getAllWorksheetsNoPager($courseid);
    }
    
    public static function getWorksheetCount() {
        return rapportenDAO::getWorksheetCount();
    }
    
    public static function getWorksheetData($wid) {
        return rapportenDAO::getWorksheetData($wid);
    }
    
    public static function addWorksheetModules($id, $modules) {
        foreach($modules as $mod) {
            rapportenDAO::insertWorksheetModule($id, $mod);
        }
    }

    public static function addWorksheetModule($id, $mod) {
            rapportenDAO::insertWorksheetModule($id, $mod);
    }

    public static function getTeacher() {
        return rapportenDAO::getTeacher();
    }
    
    public static function getStudentsFromCourse($courseid) {
        return rapportenDAO::getStudentsFromCourse($courseid);
    }
    
    public static function getCourseCount() {
        return rapportenDAO::getCourseCount();
    }
    
    public static function getStudentsCountFromCourse() {
        return rapportenDAO::getStudentsCountFromCourse();
    }
    
    public static function getTeachersByCourseId($start, $count, $course) {
        return rapportenDAO::getTeachersByCourseId($start, $count, $course);
    }
    
    public static function getTeachersCourseCount($course) {
        return rapportenDAO::getTeachersCourseCount($course);
    }
    
    public static function getAllTeachers() {
        return rapportenDAO::getAllTeachers();
    }
    
    public static function getAllTeachersFromCourse($course) {
        return rapportenDAO::getAllTeachersFromCourse($course);
    }
    
    public static function getWorksheetsFromStudent(/*$start, $count,*/ $student, $course) {
        return rapportenDAO::getWorksheetsFromStudent(/*$start, $count,*/ $student, $course);
    }
    
    public static function getTotalWorksheetsFromStudent($userid, $course) {
        return rapportenDAO::getTotalWorksheetsFromStudent($userid, $course);
    }

    public static function addTeacher($id) {
        return rapportenDAO::addTeacher($id);
    }

    public static function getLastDropdownFromUser($id) {
        return rapportenDAO::getLastDropdownFromUser($id);
    }
    
    public static function createCourse($code, $name, $description) {
        $id = rapportenDAO::insertCourse($code, $name, $description);
        if ($id != null) {
            return array(
                "id" => $id,
                "code" => $code,
                "name" => $name,
                "description" => $description);
        } else {
            return -1;
        }
    }


    public static function volgVak($courseid, $studid, $status)
    {
        $id = rapportenDAO::volgVak($courseid, $studid, $status);
                if ($id != null) {
                    return array(
                        "code" => $courseid,
                        "name" => $studid,
                        "description" => $status);
                } else {
                    return -1;
                }
    }

    public static function addTeacherToCourse($course, $teacher) {           
        $id = rapportenDAO::addTeacherToCourse($course, $teacher);
        
        if ($id != null) {
            return array(
                "course" => $course,
                "teacher" => $teacher);
        } else {
            return -1;
        }
    }
    
    public static function deleteTeacherFromCourse($courseid, $teacherid) {
        rapportenDAO::deleteTeacherFromCourse($courseid, $teacherid);
    }

    public static function addWorksheetStudent($worksheetid, $studid) {
        $id = rapportenDAO::addWorksheetStudent($worksheetid, $studid);

        if($id != null) {
            return array(
                "id" => $id,
                "worksheetid" => $worksheetid,
                "studid" => $studid
            );
        } else {
            return -1;
        }
    }

    public static function deleteCourse($id) {
        return rapportenDAO::deleteCourse($id);
    }

    public static function coursestudentdelete($courseid,$studid) {
        return rapportenDAO::coursestudentdelete($courseid,$studid);
    }

    //Verwijderen oude elementen van bepaalde werkfiche
    public static function werficheElementsDelete($wid) {
        return rapportenDAO::werficheElementsDelete($wid);
    }

    public static function copyCourse($id) {
        return rapportenDAO::copyCourse($id);
    }

    public static function updateCourse($id, $code, $name, $description) {
        if(rapportenDAO::updateCourse($id, $code, $name, $description)){
            return array(
                "id" => $id,
                "code" => $code,
                "name" => $name,
                "description" => $description);
        } else {
            return -1;
        }
    }

    public static function saveDropDownCourse($worksheetid,$worksheetname, $courseid) {
    if(rapportenDAO::saveDropDownCourse($worksheetname,$worksheetid, $courseid)){
        return array(
            "worksheetid" => $worksheetid,
            "worksheet" => $worksheetname,
            "courseid" => $courseid);
    } else {
        return -1;
    }
    }

    public static function updateWorksheet($worksheetid,$worksheetname) {
        if(rapportenDAO::updateWorksheet($worksheetname,$worksheetid)){
            return array(
                "worksheetid" => $worksheetid,
                "worksheet" => $worksheetname);
        } else {
            return -1;
        }
    }

    public static function coursestudentupdate($courseid,$studid, $status) {
        if(rapportenDAO::coursestudentupdate($courseid,$studid, $status)){
            return array(
                "course" => $courseid,
                "student" => $studid,
                "status" => $status);
        } else {
            return -1;
        }
    }

    public static function updateWorksheetProperties($id, $equip, $method, $assess, $closed) {
        if (rapportenDAO::updateWorksheetProperties($id, $equip, $method, $assess, $closed)) {
            return array(
                "equipment" => $equip,
                "method" => $method,
                "assess" => $assess,
                "closed" => $closed);
        } else {
            return -1;
        }
    }
    
    public static function assessWorksheet($date, $scores, $worksheetPassed, $worksheetfromstudent) {
        rapportenDAO::assessWorksheet($date, $worksheetPassed, $worksheetfromstudent);
        
        foreach($scores as $score) {
            rapportenDAO::assessEvaluationElement($score['elementid'], $worksheetfromstudent, $score['score']);
        }
    }
    
    public static function getCourseStructure($courseid) {
        return rapportenDAO::getCourseStructure($courseid);
    }
    
    public static function getWorksheetStructure($sheetid) {
        return rapportenDAO::getWorksheetStructure($sheetid);
    }
    
    public static function getWorksheetScores($sheetid) {
        return rapportenDAO::getWorksheetScores($sheetid);
    }
    
    public static function deleteEvaluationElement($element) {
        rapportenDAO::deleteEvaluationElement($element);
    }

    public static function getStudentListsFromUser($id) {
        return rapportenDAO::getStudentListsFromUser($id);
    }

    public static function deleteWorksheet($id) {
        return rapportenDAO::deleteWorksheet($id);
    }
    
    public static function deleteWorksheetFromUser($id, $userid) {
        return rapportenDAO::deleteWorksheetFromUser($id, $userid);
    }
    
    public static function deleteAssessedWorksheet($id) {
        return rapportenDAO::deleteAssessedWorksheet($id);
    }
    
    public static function addNewEvaluationElement($name, $description, $parent, $course) {
        $id = rapportenDAO::addNewEvaluationElement($name, $description, $parent, $course);

        if ($id != null) {
            return array(
                "id" => $id,
                "name" => $name,
                "description" => $description,
                "parent" => $parent,
                "course" => $course);
        } else {
            return -1;
        }
    }
    
    public static function updateEvaluationElement($id, $name, $description) {
        rapportenDAO::updateEvaluationElement($id, $name, $description);
    }

    public static function updateStudent($id, $firstname, $lastname, $username) {
        if(rapportenDAO::updateStudent($id, $firstname, $lastname, $username)) {
            return array(
                "id" => $id,
                "firstname" => $firstname,
                "lastname" => $lastname,
                "username" => $username,
            );
        } else {
            return -1;
        }
    }
    
    public static function exportToExcel($elements) {
        $filename = $_POST["ExportType"] . ".xls";      
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        ExportFile($elements);
        //$_POST["ExportType"] = '';
        exit();
    }
    
    public static function ExportFile($records) {
        $heading = false;
        if(!empty($records)) {
            foreach($records as $row) {
                if(!$heading) {
                    // display field/column names as a first row
                    echo implode("\t", array_keys($row)) . "\n";
                    $heading = true;
                }
                echo implode("\t", array_values($row)) . "\n";
            }
        }
        exit;
    }

    /* Feedback ------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static function addFeedback($feedback, $url) {
        rapportenDAO::addFeedback($feedback, $url);
    }
    
    /* Feedback ------------------------------------------------------------------------------------------------------------------------------------------------------*/
}
