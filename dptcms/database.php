<?php

/*
 * DPTechnics CMS
 * Database module
 * Author: Daan Pape
 * Date: 18-08-2014
 */

// Load required files
require_once('config.php');
require_once('logger.php');

/**
 * Represents an SQL database connection class.
 */
class Db {
    
    /**
     * Get the PDO database connection object
     */
    public static function getConnection() {

        // Construct the PDO adress line
        $host = Config::$dbServer;
        $port = Config::$dbPort;
        $database = Config::$dbName;
        $dsn = "mysql:host=$host;port=$port;dbname=$database";

        // Try to connect to the database
        $conn = new PDO($dsn, Config::$dbUser, Config::$dbPass);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    }

}

/*
 * Database Access Object for accessing account information
 */
class UserDAO {
    /**
     * Get all users.
     * @return stdClass the users.
     */
    public static function getAllUsers() {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT * FROM users ORDER BY lastname, firstname");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            return null;
        }
    }

    public static function updateUser($id, $firstname, $lastname, $username, $status) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("UPDATE users SET firstname = ?, lastname = ?, username = ?, status = ? WHERE id = ?");
            $stmt->execute(array($firstname, $lastname, $username, $status, $id));

            return true;
        } catch (PDOException $err) {
            echo $err;
            return false;
        }
    }

    public static function removeUserRoles($userid)
    {
        $conn = Db::getConnection();
        // Delete from user_roles (all roles)
        $stmt = $conn->prepare("DELETE FROM user_roles WHERE user_id=?");
        $stmt->execute(array($userid));

        return true;
    }



    public static function addUserRole($userid, $role)
    {
        $conn = Db::getConnection();
        // Add role for user
        $stmt = $conn->prepare("INSERT INTO user_roles(id, user_id, role_id) VALUES (NULL, ?, (SELECT id FROM roles WHERE role = ?))");
        $stmt->execute(array($userid, $role));

        return true;
    }


    /**
     * Get all users with roles.
     * @return stdClass the users.
     */
    public static function getAllUsersWithRoles() {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT r.role, r.id AS roleid, u.id AS userid, u.username, u.firstname, u.lastname, u.status FROM users AS u LEFT JOIN user_roles AS ur ON u.id = ur.user_id LEFT JOIN roles AS r ON ur.role_id = r.id ORDER BY u.lastname, u.firstname, ur.user_id, ur.role_id");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            return null;
        }
    }
    /**
     * Get account information given the users username.
     * @param type $username the username to get information from.
     * @param type $clean if clean is true the output will be filtered for public use. 
     * @return stdClass an object containing the user or null on error.
     */
    public static function getUserByUsername($username, $clean = true) {
        try {
            $conn = Db::getConnection();
            if (!$clean) {
                $stmt = $conn->prepare("SELECT * FROM users WHERE username = ?");
            } else {
                $stmt = $conn->prepare("SELECT id, username, avatar, firstname, lastname, created FROM users WHERE username = ?");
            }
            $stmt->execute(array($username));
            $data = $stmt->fetchObject();

            // Fetch an avater when it is not null
            if ($data != null) {
                $file_id = $data->avatar;
                if ($file_id != null) {
                    $data->avatar = FileDAO::getUpload($file_id);
                }
            }

            return $data;
        } catch (PDOException $err) {
            return null;
        }
    }

    /**
     * Get account information based on a user token.
     * @param type $token the users token.
     * @return stdClass the user associated with a token or null on error.
     */
    public static function getUserByToken($token) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT * FROM users WHERE activation_key = ?");
            $stmt->execute(array($token));
            return $stmt->fetchObject();
        } catch (PDOException $err) {
            return null;
        }
    }

    /**
     * Get account information based on a id.
     * @param type $id the user id
     * @return user details
     */
    public static function getEditUserById($uid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT r.role, r.id AS roleid, u.id AS userid, u.username, u.firstname, u.lastname, u.status FROM users AS u LEFT JOIN user_roles AS ur ON u.id = ur.user_id LEFT JOIN roles AS r ON ur.role_id = r.id WHERE u.id = ?");
            $stmt->execute(array($uid));
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            return null;
        }
    }

    /**
     * Get account information based on a user token.
     * @param type $role the role of the users.
     * @return stdClass the users associated with the roles.
     */
    public static function getUsersByRole($role)
    {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT r.role FROM roles r INNER JOIN user_roles ur ON ur.role_id = r.id INNER JOIN users us ON us.id = ur.user_id WHERE r.role = ?");
            $stmt->execute(array($role));
            return $stmt->fetchObject();
        } catch (PDOException $err) {
            return null;
        }
    }
    
    /**
     * Check if a username exists.
     * @param type $username the username to check.
     * @return boolean true if the user exist, false otherways and null on error.
     */
    public static function doesUserExist($username) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT COUNT(*) FROM users WHERE username = ?");
            $stmt->execute(array($username));
            return $stmt->fetchColumn() > 0;
        } catch (PDOException $err) {
            return null;
        }
    }

    /**
     * Check if an email is registered. 
     * @param type $email the email address to check.
     * @return boolean true if the email address is registered, false otherways and null on error.
     */
    public static function doesEmailExist($email) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT COUNT(*) FROM email WHERE adress = ? AND registration = 1");
            $stmt->execute(array($email));
            return $stmt->fetchColumn() > 0;
        } catch (PDOException $err) {
            return null;
        }
    }

    /**
     * Check if a token is unique. 
     * @param type $token the token to check.
     * @return boolean true if the token exists, false otherways and null on error.
     */
    public static function doesUsertokenExist($token) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT COUNT(*) FROM users WHERE activation_key = ?");
            $stmt->execute(array($token));
            return $stmt->fetchColumn() > 0;
        } catch (PDOException $err) {
            return null;
        }
    }

    /**
     * Create a new user in the database 
     * @param type $lang the users preferred language.
     * @param type $email the users primary email address.
     * @param type $username the users username, may be equal to email.
     * @param type $firstname the firstname of the user.
     * @param type $lastname the lastname of the user.
     * @param type $password the users password, must already be hashed.
     * @param type $token the users token if any. 
     * @param type $status the users activation status. 
     * @return type the userid of the newly created user or null on error.
     */
    public static function createUser($lang, $email, $username, $firstname, $lastname, $password, $token, $status) {
        try {
            // the user
            $conn = Db::getConnection();
            $stmt = $conn->prepare("INSERT INTO users (username, lang, firstname, lastname, password, activation_key, status) VALUES (?, ?, ?, ?, ?, ?, ?)");
            $stmt->execute(array($username, $lang, $firstname, $lastname, $password, $token, $status));
            $uid = $conn->lastInsertId();

            // Insert the email adress
            $stmt = $conn->prepare("INSERT INTO email (user_id, adress, type, registration) VALUES (?, ?, 'PERSONAL', 1)");
            $stmt->execute(array($uid, $email));

            // Give user rights
            $stmt = $conn->prepare("INSERT INTO user_roles (user_id, role_id) VALUES (?,1)");
            $stmt->execute(array($uid));

            return $uid;
        } catch (PDOException $err) {
            echo $err;
            return null;
        }
    }

    public static function removeUser($userid)
    {
        $conn = Db::getConnection();
        // Delete from users
        $stmt = $conn->prepare("DELETE FROM users WHERE id=?");
        $stmt->execute(array($userid));

        // Delete email
        $stmt = $conn->prepare("DELETE FROM email WHERE user_id=?");
        $stmt->execute(array($userid));

        return true;
    }

    public static function updateUserStatus($userid, $status)
    {
        try {

            $conn = Db::getConnection();
            //Update user status
            $stmt = $conn->prepare("UPDATE users SET status = ? WHERE id = ?");
            $stmt->execute(array($status, $userid));

            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not update user status', $err);
            return false;
        }
    }

    /**updateUser
     * Update the uers avatar. 
     * @param type $userid the id of the user to update
     * @param type $imageid the id of the imagefile
     * @return boolean true on success false on error
     */
    public static function updateUserProfilePicture($userid, $imageid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("UPDATE users SET avatar = ? WHERE id = ?");
            $stmt->execute(array($imageid, $userid));
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not update project', $err);
            return false;
        }
    }

    /**
     * Searches the activation token in the database and if present activates the users account. 
     * @param type $token the token to activate. 
     * @return boolean true if the user could be activated, false otherways and null on error.
     */
    public static function activateUser($token) {
        try {
            // First check if the token exists
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT COUNT(*) FROM users WHERE activation_key = ?");
            $stmt->execute(array($token));

            // Quit when the token cannot be found
            if ($stmt->fetchColumn() == 0) {
                return false;
            }

            // Remove the activation token and set account to activated
            $conn = Db::getConnection();
            $stmt = $conn->prepare("UPDATE users SET activation_key = '', status='ACTIVE' WHERE activation_key = ?");
            $stmt->execute(array($token));

            return true;
        } catch (PDOException $err) {
            echo $err;
            return null;
        }
    }

    /**
     * Get all the roles from a certain user. 
     * @param type $username the username to search the roles from. 
     * @return type an array containing all the roles associated with the username, null on error. 
     */
    public static function getUserRoles($username) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT r.role FROM roles r INNER JOIN user_roles ur ON ur.role_id = r.id INNER JOIN users us ON us.id = ur.user_id WHERE us.username = ?");
            $stmt->execute(array($username));
            return $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
        } catch (PDOException $err) {
            return null;
        }
    }

    /**
     * Get all permissions associated with role.
     * @param type $role the role you want to get the permissions from.
     * @return type an array of permissions associated with a role.
     */
    public static function getPermissionsFromRole($role) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT p.permission FROM permissions p INNER JOIN role_permissions rp ON p.id = rp.permission_id INNER JOIN roles r ON r.id = rp.role_id WHERE r.role = ?");
            $stmt->execute(array($role));
            return $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
        } catch (PDOException $err) {
            return null;
        }
    }

}

/**
 * Class mananging all email based transactions. 
 */
class EmailDAO {
    
    /**
     * Get an email in a specified language from the database.
     * @param type $tag the email tag. 
     * @param type $lang the required email language.
     * @return type an object containing email information from the database or null on error.
     */
    public static function getEmail($tag, $lang) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT * FROM emailtemplates WHERE tag = ? AND lang = ?");
            $stmt->execute(array($tag, $lang));
            return $stmt->fetchObject();
        } catch (PDOException $err) {
            return null;
        }
    }

}

/**
 * Class managing all file upload information
 */
class FileDAO {

    /**
     * Get the name of an uploaded file based on it's ID. 
     * @param type $id the ID of the uploaded file.
     * @return type the name of an uploaded file or null on error.
     */
    public static function getUpload($id) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT filename FROM uploads WHERE id = ?");
            $stmt->execute(array($id));
            return $stmt->fetchColumn();
        } catch (Exception $ex) {
            return null;
        }
    }
    
    /**
     * Put a new upload in the database.
     * @param type $name the name of the newly uploaded file.
     * @return type the ID of the new file or -1 on error.
     */
    public static function putUpload($name) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("INSERT into uploads (filename) VALUES (?)");
            $stmt->execute(array($name));
            return $conn->lastInsertId();
        } catch (Exception $ex) {
            return -1;
        }
    }
}

?>