<?php
class rapportenDAO {
    public static function getAllCourses($start, $count) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT * FROM course_rapport WHERE active = '1' LIMIT :start,:count");
            $stmt->bindValue(':start', (int) $start, PDO::PARAM_INT);
            $stmt->bindValue(':count', (int) $count, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('could not select all courses', $err);
            return null;
        }
    }

    public static function getCoursesOverviewStudent($start, $count, $userid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT id, code, name, course, student,
                                                status FROM course_rapport
                                                LEFT JOIN course_student_rapport ON course_rapport.id = course_student_rapport.course
                                                WHERE active ='1'
                                                AND student = :userid
                                                UNION
                                                SELECT id, code, name, id, null, null
                                                FROM course_rapport
                                                WHERE active ='1'
                                                AND id NOT
                                                IN (
                                                SELECT course
                                                FROM course_rapport
                                                LEFT JOIN course_student_rapport ON course_rapport.id = course_student_rapport.course
                                                WHERE active ='1'
                                                AND student = :userid
                                                )
                                LIMIT :start,:count");
            $stmt->bindValue(':start', (int) $start, PDO::PARAM_INT);
            $stmt->bindValue(':count', (int) $count, PDO::PARAM_INT);
            $stmt->bindValue(':userid', (int) $userid, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('could not select all courses', $err);
            return null;
        }
    }

    public static function getAllEvaluatieElementsFromCourse($courseid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT * FROM  evaluatie_element_rapport
                                        WHERE  course = :courseid");
            $stmt->bindValue(':courseid', (int) $courseid, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('Niet mogelijke om alle evaluatiemomenten op te halen.', $err);
            return null;
        }
    }

    public static function getAllEvaluatieElementsFromWorksheet($worksheetid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT * FROM werkfiche_evaluatie_element_rapport
                                        WHERE werkfiche = :worksheetid");
            $stmt->bindValue(':worksheetid', (int) $worksheetid, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('Niet mogelijke om alle evaluatie elementen op te halen.', $err);
            return null;
        }
    }

    public static function addWorksheetToCourse($name, $courseid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("INSERT INTO werkfiche_rapport (Name, Course)
                                    VALUES (:name, :courseid)");
            $stmt->bindValue(':name', (string) $name, PDO::PARAM_STR);
            $stmt->bindValue(':courseid', (int) $courseid, PDO::PARAM_INT);
            $stmt->execute();
            return $conn->lastInsertId();
        } catch (PDOException $err) {
            Logger::logError('could not insert into worksheets table', $err);
            return null;
        }
    }

    public static function getAllStudents() {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT * FROM users");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('could not select all students', $err);
            return null;
        }
    }

    public static function insertCourse($code, $name, $description) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("INSERT INTO course_rapport (code, name, description) VALUES (?, ?, ?)");
            $stmt->execute(array($code, $name, $description));
            // Return the id of the newly inserted item on success.
            return $conn->lastInsertId();
        } catch (PDOException $err) {
            Logger::logError('Could not create new course', $err);
            return null;
        }
    }

    public static function volgVak($courseid, $studid, $status) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("INSERT INTO course_student_rapport (course, student, status) VALUES (?, ?, ?)");
            $stmt->execute(array($courseid, $studid, $status));
            // Return the id of the newly inserted item on success.
            return $conn->lastInsertId();
        } catch (PDOException $err) {
            Logger::logError('Could not add the student to the course', $err);
            return null;
        }
    }

    public static function addTeacherToCourse($courseid, $teacherid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("INSERT INTO course_teacher_rapport (course, teacher) VALUES (?, ?)");
            $stmt->execute(array($courseid, $teacherid));
            return $conn->lastInsertId();
        } catch (PDOException $err) {
            Logger::logError('Could not add teacher', $err);
            return null;
        }
    }

    public static function insertWorksheetModule($id, $modid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("INSERT INTO werkfiche_evaluatie_element_rapport (werkfiche, element) VALUES (?, ?)");
            $stmt->execute(array($id, $modid));
        } catch (PDOException $err) {
            Logger::logError('Could not create new list', $err);
        }
    }

    //Get all courses
    public static function getAllCourse() {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT * FROM course_rapport WHERE  active = '1' ");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('Could not get courses', $err);
            return false;
        }
    }

    public static function getCoursesFromTeacher($userid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT course_rapport.id, course_rapport.name FROM course_rapport
                                    JOIN course_teacher_rapport ON course_teacher_rapport.course = course_rapport.id
                                    WHERE course_teacher_rapport.teacher = :teacherid
                                    AND course_rapport.active = 1");
            $stmt->bindValue(':teacherid', (int) $userid, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('Could not get courses', $err);
            return false;
        }
    }

    public static function getCoursesFromStudent($userid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT course_rapport.id, course_rapport.name FROM course_rapport
                                    JOIN course_student_rapport ON course_student_rapport.course = course_rapport.id
                                    WHERE course_student_rapport.student = :studentid AND course_student_rapport.status = 'Volgt'
                                    AND course_rapport.active = 1");
            $stmt->bindValue(':studentid', (int) $userid, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('Could not get courses', $err);
            return false;
        }
    }

    public static function getILTscore($courseid, $userid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT DISTINCT evaluatie_element_rapport.id as elementid, evaluatie_element_rapport.name as elementname, werkfiche_user_rapport.id as werkfiche, werkfiche_evaluatie_element_score_rapport.score as score, werkfiche_user_rapport.active as status FROM evaluatie_element_rapport
                                        JOIN werkfiche_evaluatie_element_rapport ON werkfiche_evaluatie_element_rapport.element = evaluatie_element_rapport.id
                                        JOIN werkfiche_evaluatie_element_score_rapport ON werkfiche_evaluatie_element_score_rapport.werkfiche_element = werkfiche_evaluatie_element_rapport.id
                                        JOIN werkfiche_user_rapport ON werkfiche_user_rapport.id = werkfiche_evaluatie_element_score_rapport.werkfiche_user
                                        WHERE evaluatie_element_rapport.course = :courseid AND werkfiche_user_rapport.user = :userid");
            $stmt->bindValue(':courseid', (int) $courseid, PDO::PARAM_INT);
            $stmt->bindValue(':userid', (int) $userid, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('Could not get scores', $err);
            return false;
        }
    }

    public static function getStudentsFromCourse($courseid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT users.id, users.firstname, users.lastname FROM users
                                    JOIN course_student_rapport ON course_student_rapport.student = users.id
                                    WHERE course_student_rapport.course = :course AND course_student_rapport.status = 'Volgt'");
            $stmt->bindValue(':course', (int) $courseid, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('Could not get courses', $err);
            return false;
        }
    }

    public static function getAllWorksheets($courseid, $start, $count) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT * FROM werkfiche_rapport WHERE Course = :courseid AND Active = '1' LIMIT :start, :count");
            $stmt->bindValue(':courseid', (int) $courseid, PDO::PARAM_INT);
            $stmt->bindValue(':start', (int) $start, PDO::PARAM_INT);
            $stmt->bindValue(':count', (int) $count, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('could not select all worksheets', $err);
            return null;
        }
    }

    public static function getAllWorksheetsNoPager($courseid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT * FROM werkfiche_rapport WHERE Course = :courseid AND Active = '1'");
            $stmt->bindValue(':courseid', (int) $courseid, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('could not select all worksheets', $err);
            return null;
        }
    }

    public static function getTeacher() {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT * FROM users");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('Could not find teacher', $err);
            return null;
        }
    }


    public static function addTeacher($id) {
        /*try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT * FROM users");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('Could not find teacher', $err);
            return null;
        }*/
    }

    public static function getLastDropdownFromUser($id) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT * FROM lastdropdownRapport WHERE user = :id");
            $stmt->bindValue(':id', (int) $id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('Could not get last dropdowns from the database', $err);
            return null;
        }
    }

    public static function getCourseCount() {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT COUNT(*) FROM course_rapport");
            $stmt->execute();
            return $stmt->fetchColumn();
        } catch (PDOException $err) {
            Logger::logError('Could not count all courses in the database', $err);
            return 0;
        }
    }

    public static function getWorksheetCount() {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT COUNT(*) FROM werkfiche_rapport");
            $stmt->execute();
            return $stmt->fetchColumn();
        } catch (PDOException $err) {
            Logger::logError('Could not count all worksheets in the database', $err);
            return 0;
        }
    }

    public static function getStudentsCountFromCourse() {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT COUNT(*) FROM students");
            $stmt->execute();
            return $stmt->fetchColumn();
        } catch (PDOException $err) {
            Logger::logError('Could not count all courses in the database', $err);
            return 0;
        }
    }

    public static function getTeachersByCourseId($start, $count, $course) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT users.id as 'userid', users.firstname, users.lastname FROM users
                                    JOIN course_teacher_rapport ON course_teacher_rapport.teacher = users.id
                                    WHERE course_teacher_rapport.course = :course
                                    ORDER BY users.firstname, users.lastname
                                    LIMIT :start,:count");
            $stmt->bindValue(':start', (int) $start, PDO::PARAM_INT);
            $stmt->bindValue(':count', (int) $count, PDO::PARAM_INT);
            $stmt->bindValue(':course', (int) $course, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('could not select teachers from course with id ' + $course, $err);
            return null;
        }
    }

    public static function getAllTeachers() {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT * FROM users");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('Could not find teacher', $err);
            return null;
        }
    }

    public static function getAllTeachersFromCourse($course) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT teacher FROM course_teacher_rapport
                                    WHERE course = :course");
            $stmt->bindValue(':course', (int) $course, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('Could not find teachers from course with id ' + $course, $err);
            return null;
        }
    }

    public static function getTeachersCourseCount($course) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT count(*) FROM users
                                    JOIN course_teacher_rapport ON course_teacher_rapport.teacher = users.id
                                    WHERE course_teacher_rapport.course = :course");
            $stmt->bindValue(':course', (int) $course, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchColumn();
        } catch (PDOException $err) {
            Logger::logError('Could not count all teachers from course with id ' + $course, $err);
            return 0;
                }
    }

    public static function getWorksheetsFromStudent(/*$start, $count,*/ $userid, $course) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare(" SELECT werkfiche_user_rapport.id, werkfiche_user_rapport.werkfiche, werkfiche_rapport.Name, werkfiche_user_rapport.datum, werkfiche_user_rapport.passed
                                     FROM werkfiche_user_rapport
                                     JOIN werkfiche_rapport ON werkfiche_user_rapport.werkfiche = werkfiche_rapport.id
                                     JOIN users ON werkfiche_user_rapport.user = users.id
                                     WHERE werkfiche_rapport.course = :course
                                     AND werkfiche_user_rapport.user = :userid
                                     AND werkfiche_rapport.closed = 1
                                     AND werkfiche_user_rapport.active = 1
                                     ORDER BY werkfiche_user_rapport.id");
            /*$stmt->bindValue(':start', (int) $start, PDO::PARAM_INT);
            $stmt->bindValue(':count', (int) $count, PDO::PARAM_INT);*/
            $stmt->bindValue(':course', (int) $course, PDO::PARAM_INT);
            $stmt->bindValue(':userid', (int) $userid, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('could not select all worksheets from a student for a specific course', $err);
            return null;
        }
    }

    public static function getTotalWorksheetsFromStudent($userid, $course) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT count(werkfiche_rapport.id) FROM werkfiche_user_rapport
                                    LEFT JOIN werkfiche_rapport ON werkfiche_user_rapport.werkfiche = werkfiche_rapport.id
                                    WHERE werkfiche_rapport.course = :course AND werkfiche_user_rapport.user = :userid");
            $stmt->bindValue(':course', (int) $course, PDO::PARAM_INT);
            $stmt->bindValue(':userid', (int) $userid, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchColumn();
        } catch (PDOException $err) {
            Logger::logError('Could not count all worksheets from a student for a specific course', $err);
            return 0;
        }
    }

    public static function addWorksheetStudent($worksheetid, $studid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("INSERT INTO werkfiche_user_rapport (werkfiche, user) VALUES (?,?)");
            $stmt->execute(array($worksheetid, $studid));
            return $conn->lastInsertId();
        } catch (PDOException $err) {
            Logger::logError('Could not create new coupling between a worksheet and student', $err);
            return null;
        }
    }

    public static function updateCourse($id, $code, $name, $description) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("UPDATE course_rapport SET code = ?, name = ?, description = ? WHERE id = ?");
            $stmt->execute(array($code, $name, $description, $id));
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not update course', $err);
            return false;
        }
    }

    public static function coursestudentupdate($courseid,$studid, $status) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("UPDATE course_student_rapport SET status = ? WHERE course = ? AND student = ?");
            $stmt->execute(array($status, $courseid,$studid, ));
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not update course', $err);
            return false;
        }
    }

    public static function getWorksheetScores($sheetid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT DISTINCT evaluatie_element_rapport.id, evaluatie_element_rapport.name, evaluatie_element_rapport.description, evaluatie_element_rapport.parent, werkfiche_evaluatie_element_score_rapport.score FROM werkfiche_evaluatie_element_score_rapport
                                    JOIN werkfiche_evaluatie_element_rapport ON werkfiche_evaluatie_element_rapport.id = werkfiche_evaluatie_element_score_rapport.werkfiche_element
                                    JOIN evaluatie_element_rapport ON evaluatie_element_rapport.id = werkfiche_evaluatie_element_rapport.element
                                    WHERE werkfiche_evaluatie_element_score_rapport.werkfiche_user = :sheetid
                                    ORDER BY name");
            $stmt->bindValue(':sheetid', (int) $sheetid, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('Could not get scores from worksheet with id ' + $sheetid, $err);
            return 0;
        }
    }

    public static function saveDropDownCourse($course,$userid, $courseid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("INSERT INTO lastdropdownRapport (course, courseid, user) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE course=?, courseid=?");
            $stmt->execute(array($course, $courseid, $userid, $course, $courseid));
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not update worksheet', $err);
            return false;
        }
    }

    public static function updateWorksheetProperties($id, $equip, $method, $assess,$closed) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("UPDATE werkfiche_rapport SET equipment = ?, method = ?, assessment = ?, closed = ? WHERE id = ?");
            $stmt->execute(array($equip, $method, $assess, $closed, $id));
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not update worksheet', $err);
            return false;
        }
    }

    public static function assessWorksheet($date, $worksheetPassed, $worksheetfromstud) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("UPDATE werkfiche_user_rapport SET datum = ?, passed = ? WHERE id = ?");
            $stmt->execute(array($date, $worksheetPassed, $worksheetfromstud));
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not update worksheet', $err);
            return false;
        }
    }
    
    public static function assessEvaluationElement($elementid, $worksheetfromstud, $score) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("INSERT INTO werkfiche_evaluatie_element_score_rapport(werkfiche_element, werkfiche_user, score)
                                    VALUES (?,?,?)");
            $stmt->execute(array($elementid, $worksheetfromstud, $score));
            return $conn->lastInsertId();
        } catch (PDOException $err) {
            Logger::logError('Could not insert score for element', $err);
            return null;
        }
    }

    public static function getWorksheetData($wid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT equipment, method, assessment FROM werkfiche_rapport
                                    WHERE id = :wid");
            $stmt->bindValue(':wid', (int) $wid, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('could not select worksheetdata by id ' . $wid, $err);
            return null;
        }
    }

    public static function deleteCourse($id) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("UPDATE course_rapport SET active = '0' WHERE id = :id");
            $stmt->bindValue(':id', (int) $id, PDO::PARAM_INT);
            $stmt->execute();
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not delete project', $err);
            return null;
        }
    }

    public static function coursestudentdelete($courseid,$studid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("DELETE FROM `course_student_rapport` WHERE course = :courseid AND student = :studid");
            $stmt->bindValue(':courseid', (int) $courseid, PDO::PARAM_INT);
            $stmt->bindValue(':studid', (int) $studid, PDO::PARAM_INT);
            $stmt->execute();
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not delete project', $err);
            return null;
        }
    }

    //Verwijderen oude elementen van bepaalde werkfiche
    public static function werficheElementsDelete($wid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("DELETE FROM `werkfiche_evaluatie_element_rapport` WHERE werkfiche = :wid");
            $stmt->bindValue(':wid', (int) $wid, PDO::PARAM_INT);
            $stmt->execute();
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not delete werkfiche elements', $err);
            return null;
        }
    }

    public static function deleteTeacherFromCourse($course, $teacher) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("DELETE FROM course_teacher_rapport
                                    WHERE course = :course AND teacher = :teacher");
            $stmt->bindValue(':course', (int) $course, PDO::PARAM_INT);
            $stmt->bindValue(':teacher', (int) $teacher, PDO::PARAM_INT);
            $stmt->execute();
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not delete project', $err);
            return null;
        }
    }
    
    public static function deleteWorksheet($id) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("UPDATE werkfiche_rapport SET Active = '0' WHERE id = :id");
            $stmt->bindValue(':id', (int) $id, PDO::PARAM_INT);
            $stmt->execute();
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not delete worksheet', $err);
            return null;
        }
    }
    
    public static function deleteWorksheetFromUser($id, $userid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("DELETE FROM werkfiche_user_rapport
                                    WHERE werkfiche = :id AND user = :userid");
            $stmt->bindValue(':id', (int) $id, PDO::PARAM_INT);
            $stmt->bindValue(':userid', (int) $userid, PDO::PARAM_INT);
            $stmt->execute();
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not delete worksheet from user with id ' + $userid, $err);
            return null;
        }
    }
    
    public static function deleteAssessedWorksheet($id) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("UPDATE werkfiche_user_rapport SET active = '0' WHERE id = :id");
            $stmt->bindValue(':id', (int) $id, PDO::PARAM_INT);
            $stmt->execute();
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not delete worksheet', $err);
            return null;
        }
    }

    public static function copyCourse($id) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("INSERT INTO course_rapport(code,name,description,leerkracht,active,studentlistid)
                                    SELECT code,name,description,leerkracht,active,studentlistid FROM course_rapport WHERE id = :id");
            $stmt->bindValue(':id', (int) $id, PDO::PARAM_INT);
            $stmt->execute();

            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not copy course', $err);
            return null;
        }
    }

    //set link course - teacher - studlist inactive
    public static function updateWorksheet($worksheetname,$worksheetid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("UPDATE werkfiche_rapport SET Name = ? WHERE id = ?");
            $stmt->execute(array($worksheetname,$worksheetid));
            $stmt->execute();
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not delete project', $err);
            return null;
        }
    }
    
    public static function getCourseStructure($courseid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT * FROM evaluatie_element_rapport
                                    WHERE course = :course AND active = 1
                                    ORDER BY name, parent");
            $stmt->bindValue(':course', (int) $courseid, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('could not select coursedata by id ' + $err);
            return null;
        }
    }
    
    public static function getWorksheetStructure($sheetid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("SELECT werkfiche_evaluatie_element_rapport.id, evaluatie_element_rapport.id as elementid, evaluatie_element_rapport.name, evaluatie_element_rapport.description, evaluatie_element_rapport.course, evaluatie_element_rapport.parent FROM werkfiche_evaluatie_element_rapport
                                    JOIN evaluatie_element_rapport ON evaluatie_element_rapport.id = werkfiche_evaluatie_element_rapport.element
                                    WHERE werkfiche_evaluatie_element_rapport.werkfiche = :sheetid
                                    ORDER BY name, parent");
            $stmt->bindValue(':sheetid', (int) $sheetid, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $err) {
            Logger::logError('could not select worksheetdata by id ' + $err);
            return null;
        }
    }
    
    public static function addNewEvaluationElement($name, $description, $parent, $course) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("INSERT INTO evaluatie_element_rapport(name, description, course, parent, active)
                                    VALUES (:name, :description, :course, :parent, 1)");
            $stmt->bindValue(':name', (string) $name, PDO::PARAM_STR);
            $stmt->bindValue(':description', (string) $description, PDO::PARAM_STR);
            $stmt->bindValue(':course', (int) $course, PDO::PARAM_INT);
            if (empty($parent)) {
                $stmt->bindValue(':parent', NULL);
            } else {
                $stmt->bindValue(':parent', (int) $parent, PDO::PARAM_INT);
            }
            $stmt->execute();
            return $conn->lastInsertId();
        } catch (PDOException $err) {
            Logger::logError('could not insert into evaluatie_element_rapport table', $err);
            return null;
        }
    }
    
    public static function deleteEvaluationElement($elementid) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("UPDATE evaluatie_element_rapport SET active = '0' WHERE id = :elementid");
            $stmt->bindValue(':elementid', (int) $elementid, PDO::PARAM_INT);
            $stmt->execute();
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not delete project', $err);
            return null;
        }
    }

    public static function updateEvaluationElement($id, $name, $description) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("UPDATE evaluatie_element_rapport SET name = ?, description = ? WHERE id = ?");
            $stmt->execute(array($name, $description, $id));
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not update evaluation element', $err);
            return false;
        }
    }
    
    public static function updateStudent($id, $firstname, $lastname, $username) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("UPDATE users SET firstname = ?, lastname = ?, username = ? WHERE id = ?");
            $stmt->execute(array($firstname, $lastname, $username, $id));
            return true;
        } catch (PDOException $err) {
            Logger::logError('Could not update student', $err);
            return false;
        }
    }

    /* Feedback -----------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static function addFeedback($feedback, $url) {
        try {
            $conn = Db::getConnection();
            $stmt = $conn->prepare("INSERT INTO feedback_rapport (feedback, url)
                                    VALUES (:feedback, :url)");
            $stmt->bindValue(':feedback', (string) $feedback, PDO::PARAM_STR);
            $stmt->bindValue(':url', (string) $url, PDO::PARAM_STR);
            $stmt->execute();
            return $conn->lastInsertId();
        } catch (PDOException $err) {
            Logger::logError('could not insert feedback', $err);
            return null;
        }
    }
    
    /* Feedback -----------------------------------------------------------------------------------------------------------------------------------------------------*/
}
?>
