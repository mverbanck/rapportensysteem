<?php
require_once 'apirapporten.php';
//GET routes
$app->get('/coursesrapporten', function () use ($app) {
    $app->render('templatesrapport/coursesrapporten.php');
});

$app->get('/homerapporten', function () use ($app) {
    $app->render('templatesrapport/homerapporten.php');
});

$app->get('/assessrapporten', function () use ($app) {
    $app->render('templatesrapport/assessrapporten.php');
});

$app->get('/worksheetrapporten', function () use ($app) {
    $app->render('templatesrapport/worksheetrapporten.php');
});

$app->get('/studentmanagementrapporten', function () use ($app) {
    $app->render('templatesrapport/studentmanagementrapporten.php');
});

$app->get('/api/coursemodule/:id/:name', function ($id, $name) use($app) {
    $app->render('templatesrapport/modulerapporten.php', array('courseid' => $id, 'coursename' => $name));
});
$app->get('/account/admin', function () use($app) {
    $app->render('templatesrapport/adminrapporten.php');
});

$app->get('/api/courseteachers/:id/:name', function($id, $name) use ($app) {
    $app->render('templatesrapport/courseteachersrapporten.php', array('courseteachersid' => $id, 'courseteachersname' => $name));
});

$app->get('/api/worksheet/:id/:name/:courseid/:functie', function($id, $name, $courseid, $functie) use ($app) {
    $app->render('templatesrapport/worksheetpropertiesrapporten.php', array('sheetid' => $id, 'sheetname' => $name, 'courseid' => $courseid, 'edit' => false));
});

$app->get('/api/worksheetedit/:id/:name/:courseid', function($id, $name, $courseid) use ($app) {
    $app->render('templatesrapport/worksheetpropertiesrapporten.php', array('sheetid' => $id, 'sheetname' => $name, 'courseid' => $courseid, 'edit' => true));
});

$app->get('/api/worksheetassess/:worksheetid/:worksheetname/:courseid/:userid/:username/:worksheetfromstudentid', function($workid, $workname, $courseid, $userid, $username, $worksheetfromstudid) use ($app) {
    $app->render('templatesrapport/worksheetassess.php', array('workid' => $workid, 'workname' => $workname, 'courseid' => $courseid, 'userid' => $userid, 'username' => $username, 'worksheetfromstudid' => $worksheetfromstudid));
});

$app->get('/api/worksheetscores/:sheetid/:sheetname/:studentname/:course/:date/:result', function($sheetid, $sheetname, $studentname, $course, $date, $result) use ($app) {
    $app->render('templatesrapport/worksheetscoresrapporten.php', array('sheetid' => $sheetid, 'sheetname' => $sheetname, 'studentname' => $studentname, 
                                                                        'course' => $course, 'date' => $date, 'result' => $result));
});

$app->get('/api/adminUsersCourse', function () use($app) {
    $app->render('templatesrapport/studentcoursesrapport.php');
});

$app->get('/api/studentlistrapport/info/:id', function($id) use ($app) {
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    $data = RapportAPI::getStudentListInfoFromListId($id);

    echo json_encode($data);
});

$app->get('/api/studentlistrapporten/students/:id', function($id) use($app) {
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    $data = RapportAPI::getStudentsFromStudentList($id);

    echo json_encode($data);
});

//get all courses with pages
$app->get('/api/coursesrapport/page/:pagenr', function ($pagenr) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Calculate start and count
    $pg = Pager::pageToStartStop($pagenr);
    // Get total number of courses in the database
    $pagedata = RapportAPI::getAllCourses($pg->start, $pg->count);
    $totalcourses = RapportAPI::getCourseCount();
    // Get the page
    echo json_encode(Pager::genPaginatedAnswer($pagenr, $pagedata, $totalcourses));
});

//get all courses overview form a student
$app->get('/api/coursesOverviewStudentrapport/page/:pagenr/:userid', function ($pagenr, $userid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Calculate start and count
    $pg = Pager::pageToStartStop($pagenr);
    // Get total number of courses in the database
    $pagedata = RapportAPI::getCoursesOverviewStudent($pg->start, $pg->count, $userid);
    $totalcourses = RapportAPI::getCourseCount();
    // Get the page
    echo json_encode(Pager::genPaginatedAnswer($pagenr, $pagedata, $totalcourses));
});

$app->get('/api/worksheets/page/:pagenr/:courseid', function ($pagenr, $courseid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Calculate start and count
    $pg = Pager::pageToStartStop($pagenr);
    $pagedata = RapportAPI::getAllWorksheets($courseid, $pg->start, $pg->count);
    $totalWorksheets = RapportAPI::getWorksheetCount();
    // Get the page
    echo json_encode(Pager::genPaginatedAnswer($pagenr, $pagedata, $totalWorksheets));
});
$app->get('/api/worksheets/:courseid', function ($courseid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    $data = RapportAPI::getAllWorksheetsNoPager($courseid);
    echo json_encode($data);
});
$app->get('/api/worksheetdata/:wid', function ($wid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    $data = RapportAPI::getWorksheetData($wid);
    echo json_encode($data);
});

$app->get('/api/getAllEvaluatieElementsFromCourse/:courseid', function ($courseid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    $data = RapportAPI::getAllEvaluatieElementsFromCourse($courseid);
    echo json_encode($data);
});

$app->get('/api/getTeachersByCourseId/page/:pagenr/:course', function($pagenr, $course) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Calculate start and count
    $pg = Pager::pageToStartStop($pagenr);
    $pagedata = RapportAPI::getTeachersByCourseId($pg->start, $pg->count, $course);
    $teachercount = RapportAPI::getTeachersCourseCount($course);
    // Get the page
    echo json_encode(Pager::genPaginatedAnswer($pagenr, $pagedata, $teachercount));
});
$app->get('/api/getAllTeachers', function () use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    $data = RapportAPI::getAllTeachers();
    echo json_encode($data);
});
$app->get('/api/getCourseTeachers/:courseid', function ($courseid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    $data = RapportAPI::getAllTeachersFromCourse($courseid);
    echo json_encode($data);
});
//get all worksheets from a specific user and course
$app->get('/api/getworksheetsfromstudent/page/:student/:course', function ( $studentid, $course) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Calculate start and count
  //  $pg = Pager::pageToStartStop($pagenr);
    $pagedata = RapportAPI::getWorksheetsFromStudent(/*$pg->start, $pg->count,*/ $studentid, $course);
   // $totalworksheets = RapportAPI::getTotalWorksheetsFromStudent($studentid, $course);
    // Get the page
  //  echo json_encode(Pager::genPaginatedAnswer($pagenr, $pagedata, $totalworksheets));
    echo json_encode($pagedata);
});

//getcriterias from doelstelling
$app->get('/api/criteriarapport/:doelstellingId', function ($compid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Get all criteria by competence
    $pagedata = RapportAPI::getcriteriaBydoelstelling($compid);
    echo json_encode($pagedata);
});

$app->get('/api/coursestructure/:courseid', function($courseid) use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');
    echo json_encode(RapportAPI::getCourseStructure($courseid));
});

$app->get('/api/worksheetstructure/:sheetid', function($sheetid) use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');
    echo json_encode(RapportAPI::getWorksheetStructure($sheetid));
});

$app->get('/api/worksheetscores/:sheetid', function($sheetid) use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');
    echo json_encode(RapportAPI::getWorksheetScores($sheetid));
});

$app->get('/api/coursedrop', function () use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Get all courses
    $pagedata = RapportAPI::getAllCourse();
    echo json_encode($pagedata);
});

$app->get('/api/getcoursesfromteacher/:userid', function ($userid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Get all courses
    $pagedata = RapportAPI::getCoursesFromTeacher($userid);
    echo json_encode($pagedata);
});
$app->get('/api/getcoursesfromstudent/:userid', function ($userid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Get all courses
    $pagedata = RapportAPI::getCoursesFromStudent($userid);
    echo json_encode($pagedata);
});

$app->get('/api/getILTscore/:courseid/:userid', function ($courseid, $userid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Get all courses
    $pagedata = RapportAPI::getILTscore($courseid, $userid);
    echo json_encode($pagedata);
});

//get teacher from database
$app->get('/api/getteacherrapport', function () use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Get all teachers
    $pagedata = RapportAPI::getTeacher();
    echo json_encode($pagedata);
});
//add teacher to dropdown
$app->get('/api/teacherrapport/:id', function ($courseid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    $pagedata = RapportAPI::addTeacher($courseid);
    echo json_encode($pagedata);
});
//get last selected dropdown list
$app->get('/api/lastdropdownrapporten/:id', function($id) use ($app) {
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    $data = RapportAPI::getLastDropdownFromUser($id);
    echo json_encode($data);
});

//GET all students from a course
$app->get('/api/getstudentsfromcourse/:courseid', function ($courseid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Get all students by courseid
    $pagedata = RapportAPI::getStudentsFromCourse($courseid);
    echo json_encode($pagedata);
});

//get all students
$app->get('/api/allstudents', function () use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    
    $data = RapportAPI::getAllStudents();
    echo json_encode($data);
});

//PUT routes
$app->put('/api/courseupdate/:id', function($id) use ($app){
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    // Update the existing course
    echo json_encode(RapportAPI::updateCourse(
        $id, $app->request->post('code'), $app->request->post('name'), $app->request->post('description')));
});

$app->delete('/api/coursestudentdelete/:courseid/:studid', function ($courseid,$studid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    echo json_encode(RapportAPI::coursestudentdelete($courseid,$studid));
});

//Verwijderen oude elementen van bepaalde werkfiche
$app->delete('/api/werficheElementsDelete/:wid', function ($wid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    echo json_encode(RapportAPI::werficheElementsDelete($wid));
});


$app->put('/api/coursestudentupdate/:courseid/:studid/:status', function($courseid,$studid,$status) use ($app){
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    // Update
    echo json_encode(RapportAPI::coursestudentupdate($courseid,$studid,$status));
});

$app->put('/api/updateWorksheet/:worksheetid/:worksheetname', function($worksheetid,$worksheetname) use ($app){
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    // Update
    echo json_encode(RapportAPI::updateWorksheet($worksheetid,$worksheetname));
});

$app->put('/api/saveDropDownCourse/:worksheetid/:worksheetname/:courseid', function($worksheetid,$worksheetname, $courseid) use ($app){
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    // Update
    echo json_encode(RapportAPI::saveDropDownCourse($worksheetid,$worksheetname, $courseid));
});

$app->put('/api/worksheetproperties/:id/:assess/:closed', function($id, $assess,$closed) use ($app){
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    // Update the existing worksheetproperties
    echo json_encode(RapportAPI::updateWorksheetProperties($id, $app->request->post('equipment'), $app->request->post('method'), $assess,$closed));
});

$app->put('/api/worksheetpropertiesNoMeta/:id/:equipment/:method/:assess/:closed', function($id, $equipment, $method, $assess,$closed) use ($app){
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    // Update the existing worksheetproperties
    echo json_encode(RapportAPI::updateWorksheetProperties($id, $equipment, $method, $assess,$closed));
});

$app->post('/api/assessworksheet/:worksheetfromstudid', function($worksheetfromstudid) use ($app){
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    echo json_encode(RapportAPI::assessWorksheet($app->request->post('date'), $app->request->post('scores'), $app->request->post('worksheetPassed'), $worksheetfromstudid));
});
//POST routes
$app->post('/api/courserapport', function () use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Insert the data
    echo json_encode(RapportAPI::createCourse($app->request->post('code'), $app->request->post('name'), $app->request->post('description')));
});

$app->post('/api/volgVak/:courseid/:studid/:status', function ($courseid, $studid,$status) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Insert the data
    echo json_encode(RapportAPI::volgVak($courseid, $studid, $status));
});


$app->post('/api/addteachertocourse/:courseid/:teacherid', function ($courseid, $teacherid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Insert the data
    echo json_encode(RapportAPI::addTeacherToCourse($courseid, $teacherid));
});

$app->post('/api/addworksheet/:courseid', function ($courseid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Insert the data
    echo json_encode(RapportAPI::addWorksheetToCourse($app->request->post('name'), $courseid));
});

$app->post('/api/addworksheetNoMeta/:worksheetname/:courseid', function ($name, $courseid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    // Insert the data
    echo json_encode(RapportAPI::CopyWorksheet($name, $courseid));
});

$app->post('/api/addNewEvaluationElement/:courseid', function($courseid) use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');
    echo json_encode(RapportAPI::addNewEvaluationElement($app->request->post('name'), $app->request->post('description'), $app->request->post('parent'), $courseid));
});

$app->post('/api/deleteEvaluationElement', function() use ($app) {
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    echo json_encode(RapportAPI::deleteEvaluationElement($app->request->post('element')));
});

$app->delete('/api/deleteTeacherFromCourse/:courseid/:teacherid', function($courseid, $teacherid) use ($app) {
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    echo json_encode(RapportAPI::deleteTeacherFromCourse($courseid, $teacherid));
});

$app->post('/api/addworksheetstudent', function() use($app) {
    //Use json header
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    //Insert the data
    echo json_encode(RapportAPI::addWorksheetStudent($app->request->post('worksheetid'), $app->request->post('studid')));
});
$app->post('/api/worksheetstudentListcouple', function() use($app) {
    //Use json header
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    //Insert the data
    echo json_encode(RapportAPI::createWorksheetStudentListCouple($app->request->post('worksheetid'), $app->request->post('studlistid')));
});

$app->post('/api/worksheetmodules', function() use($app) {
    //Use json header
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    //Insert the data
    echo json_encode(RapportAPI::addWorksheetModules($app->request->post('id'), $app->request->post('modules')));
});

$app->post('/api/exportToExcel', function() use($app) {
    //Use json header
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    //Insert the data
    echo json_encode(RapportAPI::exportToExcel($app->request->post('elements')));
});

/* Feedback ----------------------------------------------------------------------------------------------------------------------------------------------------------------*/
$app->post('/api/addFeedback', function() use($app) {
    //Use json header
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    //Insert the data
    echo json_encode(RapportAPI::addFeedback($app->request->post('feedback'), $app->request->post('url')));
});
/* Feedback ----------------------------------------------------------------------------------------------------------------------------------------------------------------*/

// API DELETE routes
$app->delete('/api/coursedelete/:id', function ($id) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    echo json_encode(RapportAPI::deleteCourse($id));
});

$app->delete('/api/coursestudentdelete/:courseid/:studid', function ($courseid,$studid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    echo json_encode(RapportAPI::coursestudentdelete($courseid,$studid));
});

$app->delete('/api/deleteworksheet/:id', function ($id) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    echo json_encode(RapportAPI::deleteWorksheet($id));
});

$app->delete('/api/deleteworksheetfromuser/:id/:userid', function ($id, $userid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    echo json_encode(RapportAPI::deleteWorksheetFromUser($id, $userid));
});

$app->delete('/api/deleteassessedworksheet/:id', function($id) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    echo json_encode(RapportAPI::deleteAssessedWorksheet($id));
});

$app->put('/api/elementupdate/:id', function($id) use ($app){
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    echo json_encode(RapportAPI::updateEvaluationElement($id, $app->request->post('name'), $app->request->post('description')));
});

$app->put('/api/studentrapport/:id', function($id) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    // Update the existing resource
   echo json_encode(RapportAPI::updateStudent(
        $id, $app->request->post('firstname'), $app->request->post('lastname'), $app->request->post('username')));
});


$app->post('/api/coursecopy/:id', function ($id) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    echo json_encode(RapportAPI::copyCourse($id));
});

?>
