<?php
session_start();

require_once 'Slim/Slim.php';
require_once 'api.php';
require_once 'dptcms/pagination.php';
require_once 'dptcms/logger.php';
require_once 'dptcms/email.php';
require_once 'dptcms/security.php';
require_once 'dptcms/fileupload.php';

\Slim\Slim::registerAutoloader();

/* Instatiate application */
$app = new \Slim\Slim(array(
    'cookies.encrypt' => true
        ));
$app->setName('Rapportensysteem');

/* Add security middleware */
class DPTSecurityMiddleware extends \Slim\Middleware
{
    /* 
     * This function is called by the slim application for all requests
     */
    public function call()
    {
        // Get the reference to the application
        $app = $this->app;

        // Get the application request without trailing slashes 
        $requesturi = ltrim($app->request->getPathInfo(), '/');

        if($requesturi != 'unauthorized') {
            // Check if the user is authorized to execute this request
            if(!Security::isUserAuthorized($requesturi)) {
                $app->redirect('/unauthorized');
            }
        }

        // Run the inner middleware and application
        $this->next->call();
    }
}

$app->add(new DPTSecurityMiddleware());

// GET routes
$app->get('/', function () use ($app) {
    $app->render('templatesrapport/homerapporten.php');
});
$app->get('/register', function () use ($app) {
    $app->render('register.php');
});
$app->get('/account', function() use ($app) {
    $app->render('account.php');
});
$app->get('/unauthorized', function() use ($app) {
    $app->render('unauthorized.php');
});
$app->get('/activate/:token', function ($token) use ($app) { 
    // Try to activate user 
    $status = Security::activateUser($token);
    $app->render('activate.php', array('status' => $status));
});
$app->get('/logout', function() use ($app) {
    Security::logoutUser();
});

// POST routes
$app->post('/login/:email', function ($email) use($app) {	
    $app->response->headers->set('Content-Type', 'application/json');

    // Try to login the user 
    $response = Security::loginUser($email, $_POST['password']);
    if($response !== true){
        // Login failed
        $app->response->setStatus(401);

        // Send error message
        echo json_encode(array('error' => $response));
    } else {
        // Login success
        echo '{}';
    }
});

$app->post('/checkemail', function() use($app) {
    $app->response->headers->set('Content-Type', 'application/json');

    // Check if the email address is unique
    $unique = Security::isEmailUnique($_POST['email']);
    echo json_encode(array('valid' => $unique));
});

$app->post('/register', function() use($app){

    $filter = array(
        'firstname' => array(
            'filter' => FILTER_SANITIZE_STRING,
            'flags' => FILTER_FLAG_NO_ENCODE_QUOTES
            ),
        'lastname'  => array(
            'filter' => FILTER_SANITIZE_STRING,
            'flags' => FILTER_FLAG_NO_ENCODE_QUOTES
            ),
        'email'     => FILTER_VALIDATE_EMAIL,
        'status'    => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array(
                'regexp' => '/^WAIT_ACTIVATION|ACTIVE|DISABLED$/'
                )
            ),
        'lang'      => FILTER_SANITIZE_STRING
    );
    
    $fPOST = filter_input_array(INPUT_POST, $filter, false);
    
    foreach($fPOST as $name => $value)
    {
        if($value === null)
        {
            throw new \Exception("Validation failed on POST field '{$name}'");
        }
    }
    
    if(@$_POST['skipEmailVerification'])
    {
        $skipEmailVerification = true;
    }
    else
    {
        $skipEmailVerification = false;
    }
    
    if(!Security::registerUser($fPOST['lang'], $fPOST['firstname'], $fPOST['lastname'], $fPOST['email'], $fPOST['email'], $_POST['pass'], $_POST['passconfirm'], $skipEmailVerification)) {
        // Registration failed, bad request
        $app->response->setStatus(400);
    }

});


// API GET routes

$app->get('/api/currentuser', function() use ($app) {
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    // Get all courses by the trainingsid
    $userdata = GraderAPI::getUserData(Security::getLoggedInUsername());

    echo json_encode($userdata);
});
$app->get('/api/edituser/:id', function($id) use ($app) {
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    // Get all user data by id
    $userdata = GraderAPI::getEditUserDataById($id);

    echo json_encode($userdata);
});

$app->post('/api/saveedit/:id', function($id) use ($app) {

    // Try to edit the user
    
    $filter = array(
        'firstname' => array(
            'filter' => FILTER_SANITIZE_STRING,
            'flags' => FILTER_FLAG_NO_ENCODE_QUOTES
            ),
        'lastname'  => FILTER_SANITIZE_STRING,
        'email'     => FILTER_VALIDATE_EMAIL,
        'status'    => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array(
                'regexp' => '/^WAIT_ACTIVATION|ACTIVE|DISABLED$/'
                )
            )
    );
    
    $fPOST = filter_input_array(INPUT_POST, $filter, true);

    foreach($fPOST as $name => $value)
    {
        if($value === null)
        {
            throw new \Exception("Validation failed on POST field '{$name}'");
        }
    }
    
    if(!GraderAPI::updateUser($id, $fPOST['firstname'], $fPOST['lastname'], $fPOST['email'], $fPOST['status'])) {
        // Edit failed, bad request
        $app->response->setStatus(400);
    }
});

$app->get('/api/removeroles/:id', function($id) use ($app) {
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    echo json_encode(GraderAPI::removeUserRoles($id));
});

$app->post('/api/addrole/:id', function($id) use ($app) {
    // Try to edit the user
    if(!GraderAPI::addUserRole($id, $_POST['currentRight'])) {
        // Edit failed, bad request
        $app->response->setStatus(400);
    }
});

$app->get('/api/project/getAllData/:id', function($id) use($app) {
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    $allData = GraderAPI::getAllDataFromProject($id);

    echo json_encode($allData);

});

$app->get('/api/projectstructure/:id', function($id) use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');
    echo json_encode(GraderAPI::getAllDataFromProject($id));
});

$app->get('/api/loggedinuser', function() use ($app)
{
    $response = $app->response();
    $response->headers('Content-Type','application/json');
    echo json_encode(GraderAPI::getLoggedInId());
});

$app->get('/api/allusers/', function() use ($app)
{
    $response = $app->response();
    $response->headers('Content-Type','application/json');
    echo json_encode(GraderAPI::getAllUsersData(Security::getLoggedInId()));
});

$app->get('/api/removeuser/:userid', function($userid) use ($app) {
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    echo json_encode(GraderAPI::removeUser($userid));
});

$app->get('/api/updateuserstatus/:userid/:status', function($userid, $status) use ($app) {
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    echo json_encode(GraderAPI::updateUserStatus($userid, $status));
});

$app->get('/api/alluserswithroles/', function() use ($app)
{
    $response = $app->response();
    $response->headers('Content-Type','application/json');
    echo json_encode(GraderAPI::getAllUsersWithRolesData(Security::getLoggedInId()));
});

// API POST routes
$app->post('/api/project/:id', function ($courseid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    // Insert the data
    echo json_encode(GraderAPI::createProject(
                    $courseid, $app->request->post('code'), $app->request->post('name'), $app->request->post('description')));
});

$app->post('/api/student/:id', function ($listid) use ($app) {
    // Use json headers
    $response = $app->response();
    $response->header('Content-Type', 'application/json');

    // Insert the data
    echo json_encode(GraderAPI::createStudentAndCoupleToListId(
        $listid, $app->request->post('username'), $app->request->post('firstname'), $app->request->post('lastname')));
});

$app->post('/api/account/avatar', function() use ($app) {
    //Use json header
    $response = $app->response();
    $response->header('Content-Type', 'application/json');
    
    echo json_encode(GraderAPI::updateProfilePicture($_POST['pictureid']));
});

$app->post('/api/upload', function() use($app){
    $app->response->headers->set('Content-Type', 'application/json');
    echo json_encode(FileUpload::uploadFile());
});

$app->post('/api/csv/studentlist', function() use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');
    
    //TODO: parse CSV 
    echo json_encode(CSVParser::parseCSV($_POST['fileid']));
});

/* Rapporten routering */
require_once 'indexrapporten.php';

/* Admin router */
require_once 'admin.php';


/* Run the application */
$app->run();