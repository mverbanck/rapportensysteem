// View model for the page
function pageViewModel(gvm) {
    gvm.app  = ko.observable("Home");
    gvm.title = ko.observable("Rapportensysteem Deeltijds Onderwijs");
    gvm.projectname = ko.observable("Rapportensysteem");
    
    gvm.scores = ko.observableArray([]);
    
    gvm.clearStructure = function() {
        gvm.scores([]);
    }
}

function getScores() {
    viewModel.clearStructure();
    
    $.getJSON("/api/worksheetscores/" + sheetid, function(data){
        console.log(data);
        $.each(data, function(i, item){
            if (item.parent === null) {
                viewModel.scores.push({id: item.id, name: item.name, description: item.description, score: item.score, level: 1});
                insertLevelIntoStructure(item.id, 1, data, 2);
            };
            console.log("a getscore for each loop");
        });
        console.log("getscores success");
    });
}

function insertLevelIntoStructure(parentid, parentlevel, data, level) {
    console.log(level);
    if (level <= 5) {
        $.each(data, function(j, item) {
            if (item.parent === parentid && parentlevel === level - 1) {
                viewModel.scores.push({id: item.id, name: item.name, description: item.description, score: item.score, level: level});
                insertLevelIntoStructure(item.id, level, data, level + 1);
                console.log("an insert for each successful loop");
            }
        });
    }
    console.log("insertintostructure success");
}

function initPage() {
    getScores();
    
    $('#goBackBtn').on('click', function() {
        window.history.back();
    });
}