$( document ).ready(function() { $('#feedbackForm').on('submit', function(e) {
        e.preventDefault();
        var feedback = $('#feedback').val();
        var url = window.location.href;
        if (feedback !== "") {
            $.ajax({
                url: "/api/addFeedback",
                type: "POST",
                data: {feedback: feedback, url: url},
                success: function() {
                    $('#feedback').val("");
                    window.alert('Bedankt voor de feedback!');
                    console.log('succes');
                },
                error: function() {
                    console.log('failure');
                }
            });
        }
    });
});