currentstudentid = 0;
var geslaagd = document.getElementById("geslaagd");
var volgt = document.getElementById("volgt");
var volgtNiet = document.getElementById("volgtNiet");
var rowStatus =  document.getElementsByClassName("rowStatus");
// View model for the courses page
function pageViewModel(gvm) {
    gvm.app  = ko.observable("Vakken");
    gvm.title = ko.observable("Rapportensysteem Deeltijds Onderwijs");
    gvm.projectname = ko.observable("Rapportensysteem");

    gvm.availableLocations = ko.observableArray([]);
    gvm.availableTrainings = ko.observableArray([]);
    gvm.availableCourses = ko.observableArray([]);

    gvm.availableTeacher = ko.observableArray([]);
    gvm.currentteacherid = null;

    // The table data observable array
    gvm.tabledata = ko.observableArray([]);

    // Add data to the table
    gvm.addTableData = function(id, code, name, stat) {
        // Push data

        if (stat === '' || stat === null) {
            stat = 'Volgt niet'
        }

        var tblOject = {tid: id, tcode: code, tname: name, tstat: stat};
        gvm.tabledata.push(tblOject);

        // Attach add handler to add button
        $('#addbtn-' + id).bind('click', function(event, data){
            // add the table item
            volgVak(tblOject);
            event.stopPropagation();
        });

        // Attach delete handler to delete button
        $('#delbtn-' + id).bind('click', function(event, data){
            // del course followingen
            deleteTableItem(tblOject);
            event.stopPropagation();
        });

        $('#sucbtn-' + id).bind('click', function(event, data){
            updateCourse(tblOject,"Geslaagd");
            event.stopPropagation();
        });


        // Attach update handler to reset button
        $('#resbtn-' + id).bind('click', function(event, data){
            // Update the table item
            console.log("resetbtn clicked");
            updateCourse(tblOject,"Volgt");
            event.stopPropagation();
        });

        //Verbergen functies die niet nodig zijn in deze situatie
        if (stat === 'Volgt niet') {

            $('#delbtn-' + id).hide();
            $('#sucbtn-' + id).hide();
            $('#resbtn-' + id).hide();
        }
        else if (stat === 'Volgt') {
            $('#addbtn-' + id).hide();
            $('#resbtn-' + id).hide();
        }
        else {
           $('#addbtn-' + id).hide();
            $('#delbtn-' + id).hide();
            $('#sucbtn-' + id).hide();
        }
    }

    gvm.clearTable = function() {
        gvm.tabledata.removeAll();
    }
}

//get all students
function getAllStudents() {
    studcombo = [];
    $.getJSON('/api/allstudents', function(data) {
        $.each(data, function(i, item) {
            studcombo.push({ label:item.firstname + " " + item.lastname, id: item.id });
        });
    });
    return studcombo;
}

//status volgen toevoegen met vakid en studid
function volgVak(tblOject) {
    status = "Volgt"
    $.ajax({
        url: "/api/volgVak/" + tblOject.tid + "/" + currentstudentid + "/" + status,
        type: "POST",
        success: function(data) {
            loadTablePage(1,currentstudentid);
            console.log("toevoegen geslaagd")

        },
        error: function(data) {
            console.log("vak toevoegen " + data + " is niet gelukt.")
        }
    });
}

//Vak niet langer volgen
function deleteTableItem(tblOject) {
    //showYesNoModal("Bent u zeker dat u deze persoon voor " + tblOject.tname +" wilt uitschrijven? \r\n Let op: verwijderde vakken blijven hun punten behouden.", function(val){
      //  if(val){
            $.ajax({
                url: "/api/coursestudentdelete/" + tblOject.tid + "/" + currentstudentid,
                type: "DELETE",
                success: function() {
                    loadTablePage(1,currentstudentid);

                }
            });
        //}
    //});
}

/*
 * update studentcourse status to geslaagd
 */
function updateCourse(tblOject,status) {
    $.ajax({
        url: "/api/coursestudentupdate/" + tblOject.tid + "/" + currentstudentid + "/" + status,
        type: "PUT",
        success: function(data) {
            loadTablePage(1,currentstudentid);

        },
        error: function(data) {
            console.log("Geslaagd aanduiden mislukt.")
        }
    });
}

/*
 * Load page of the table
 */
function loadTablePage(pagenr,studnr)
{
    $.getJSON('/api/coursesOverviewStudentrapport/page/' + pagenr+ '/' + studnr, function(data){

        /* Clear current table page */
        viewModel.clearTable();
        $(".table").find('tbody').empty();
        // Load table data
        $.each(data.data, function(i, item) {
            viewModel.addTableData(item.id, item.code, item.name, item.status);
            updateTableAfterCheckboxGeslaagd();
            updateTableAfterCheckboxVolgtNiet();
            updateTableAfterCheckboxVolgt();
        });
        $(".table").trigger('update');


        $('[data-toggle="tooltip"]').tooltip();
        /* Let previous en next buttons work */
        if(data.prev == "none"){
            $('#pager-prev-btn').addClass('disabled');
        } else {
            $('#pager-prev-btn').removeClass('disabled');
            $('#pager-prev-btn a').click(function(){
                loadTablePage(data.prev);
            });
        }

        if(data.next == "none"){
            $('#pager-next-btn').addClass('disabled');
        } else {
            $('#pager-next-btn').removeClass('disabled');
            $('#pager-next-btn a').click(function(){
                loadTablePage(data.next);
            });
        }

        // Number of pager buttons
        var numItems = $('.pager-nr-btn').length;

        /* Calculate for the pager buttons */
        var lowPage = Math.floor(pagenr/numItems) + 1;

        $('.pager-nr-btn').each(function() {
            /* calculate current page number */
            var thispagenr = lowPage++;

            /* Add the page number */
            $(this).html('<a href="#">' + thispagenr + '</a>');

            /* Add active class to current page */
            if(thispagenr == pagenr) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }

            /* Disable inactive classes and bind handlers to active classes */
            if(thispagenr > data.pagecount) {
                $(this).addClass('disabled');
            } else {
                /* Add click listener for button */
                $(this).click(function() {
                    loadTablePage(thispagenr);
                });
            }
        });
    });

}
//add eventhandlers to the checkboxes
function attachCheckboxHandlers(){

    volgt.addEventListener('click',updateTableAfterCheckboxVolgt);
    volgtNiet.addEventListener('click',updateTableAfterCheckboxVolgtNiet);
   geslaagd.addEventListener('click',updateTableAfterCheckboxGeslaagd);



}
//filteroption functions
function updateTableAfterCheckboxVolgt(){

// if volgt is checked then this will show all the courses the student follows the course


        $(".rowStatus").each(function(){
            if(this.innerHTML == "Volgt")
            {  if(volgt.checked ) {
                $(this).parent().show();
            }
                else{$(this).parent().hide();
            }}
        })


}
function updateTableAfterCheckboxVolgtNiet(){

        $(".rowStatus").each(function(){
            if(this.innerHTML == "Volgt niet")
            { if(volgtNiet.checked) {
                $(this).parent().show();
            }else{
                $(this).parent().hide();
            }}
        })



}
function updateTableAfterCheckboxGeslaagd(){

        $(".rowStatus").each(function(){
            if(this.innerHTML == "geslaagd")
            {  if(geslaagd.checked ) {
                $(this).parent().show();
            }
            else{$(this).parent().hide();
            }}
        })

    

}
$(function() {
    $( "#studentsComplete" ).autocomplete({
        delay: 0,
        source: getAllStudents(),
        select: function(event, ui) {
            currentstudentid = ui.item.id;
            loadTablePage(1,ui.item.id);

        }
    });

});


function initPage() {
    $.getJSON('/api/currentuser', function (data) {
        viewModel.userId = data.id;
    });

    $.widget( "custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function( ul, items ) {
            var self = this
            $.each( items, function( index, item ) {
                self._renderItem( ul, item );
            });
        }
    });
    $(".table").tablesorter();

    //leegmaken leerling bij klik
    $( "#studentsComplete" ).click(function (e) {
        e.preventDefault();
        $("#studentsComplete").val("");
    });
    attachCheckboxHandlers()

 }