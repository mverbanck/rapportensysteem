/**
 * module class
 */
function evaluatieElement(id, name, description, course, parent, number) {        //overkoepelende naam voor modules, doelstellingen, criteria
    return {
        id: ko.observable(id),
        name: ko.observable(name),
        description: ko.observable(description),
        course: ko.observable(course),
        parent: ko.observable(parent),
        children: ko.observableArray([]),
        number: ko.observable(number)
    };
}

// View model for the courses page
function pageViewModel(gvm) {    
    // Page specific i18n bindings
    gvm.app  = ko.observable("Studenten beoordelen");
    gvm.title = ko.observable("Rapportensysteem Deeltijds Onderwijs");
    gvm.projectname = ko.observable("Rapportensysteem");

    gvm.evaluatieElementen = ko.observableArray([]);
    gvm.parents = ko.observableArray([]);     //wordt gebruikt voor de autocomplete
    gvm.rawData = ko.observableArray([]);
    
    gvm.clearStructure = function() {
        gvm.evaluatieElementen([]);
        gvm.parents([]);
        gvm.rawData([]);
    }
}

function fetchCourseStructure() {
    viewModel.clearStructure();
    
    $.getJSON("/api/coursestructure/" + courseid, function(data){
        viewModel.rawData = data;
        $.each(data, function(i, item){
            if (item.parent === null) {    //eerst alle hoofdelementen (null als parent) ophalen
                viewModel.evaluatieElementen.push(new evaluatieElement(item.id, item.name, item.description, item.course, item.parent, viewModel.evaluatieElementen().length + 1));
                viewModel.parents.push({id: item.id, name: item.name, number: viewModel.parents().length + 1});
            };
        });
        insertLevelIntoStructure(viewModel.evaluatieElementen(), data, 2);
        manageEventHandlers(data);
    });
}

function insertLevelIntoStructure(collection, data, level) {
    if (level <= 5) {
        $.each(collection, function(i, element) {
            $.each(data, function(j, item) {    //per hoofdparent gaan kijken of hij children heeft
                if (item.parent === element.id()) {
                    var number = element.number() + '.' + parseInt(element.children().length + 1);
                    element.children.push(new evaluatieElement(item.id, item.name, item.description, item.course, item.parent, number));
                    viewModel.parents.push({id: item.id, name: item.name, number: number});
                }
            });
            insertLevelIntoStructure(element.children(), data, level + 1);
        });
    }
}

function getParents() {
    var parents = [];
    $.each(viewModel.parents(), function(i, item) {
        parents.push(item.number + ' ' + item.name);
    });
    return parents;
}

function getParentId(parent) {
    var id = null;   //null, indien hoofdelement
    $.each(viewModel.parents(), function(i, item) {
        if ((item.number + ' ' + item.name) === parent) {
            id = item.id;
        }
    });
    return id;
}

function manageEventHandlers(data) {
    $.each(data, function(i, item) {
        //removing
        $('#removebtn-' + item.id).on('click', function() {
            deleteEvaluationElement(item.id);               //TODO: ipv een ajax call op te roepen per element die verwijderd moet worden, ze
            console.log(item.id);                           //bundelen in een array, en die doorgeven aan 1 ajax call -> betere performantie
            $.each(data, function(j, child) {
                if (child.parent === item.id) {
                    $('#removebtn-' + child.id).trigger('click');
                }
            });
      location.reload();  });
        //editing
        $('#editbtn-' + item.id).on('click', function() {
            showEditElementModal(item.name, item.description, item.id);
        });
    });
}

function addNewEvaluationElement(name, description, parent) {
    $.ajax({
        url: "/api/addNewEvaluationElement/" + courseid,
        type: "POST",
        data: {name: name, description: description, parent: parent},
        success: function(data) {
            $('input[name="name"]').val("");
            $('input[name="description"]').val("");
            $('input[name="parent"]').val("");
            fetchCourseStructure();    //veranderen door het nieuwe element te inserten (new evaluationElement())
            console.log('succes');
        },
        error: function(data) {
            console.log('failure');
        }
    });
}

function editEvaluationElement(id, serialData, callback) {
    $.ajax({
        url: "/api/elementupdate/" + id,
        type: "PUT",
        data: serialData,
        success: function(data) {
            fetchCourseStructure()
            callback(true);
        },
        error: function(data) {
            callback(false);
        }
    });
}

function deleteEvaluationElement(element) {
    $.ajax({
        url: "/api/deleteEvaluationElement",
        type: "POST",   //geen DELETE link, want dan kan je geen data meegeven      
        data: {element: element},
        success: function() {
            console.log("succes");
        },
        error: function() {
            console.log('failure');
        }
    });
}

function showEditElementModal(name, description, eid)
{
  resetGeneralModal();
    setGeneralModalTitle("Pas competentie aan");
    setGeneralModalBody('<form id="editelementform"> \
            <div class="form-group"> \
                <input type="text" class="form-control input-lg" placeholder="naam" name="name" value="' + name + '"> \
            </div> \
            <div class="form-group"> \
                <input type="text" class="form-control input-lg" placeholder="beschrijving" name="description" value="' + description + '"> \
            </div> \
        </form>');
    $.getJSON();

    addGeneralModalButton("Opslaan", function(){
        editEvaluationElement(eid, $('#editelementform').serialize(), function(result){
            hideModal();
        });
    });

    addGeneralModalButton("Annuleer", function(){
        hideModal();
    });

    showGeneralModal();
}

function exportTableToExcel() {
    $.ajax({
        url: "/api/exportToExcel",
        type: "POST",    
        data: {elements: viewModel.rawData},
        success: function() {
            console.log("succes");
        },
        error: function() {
            console.log('failure');
        }
    });
}

function initPage() {    
    fetchCourseStructure();
    
    $('#parentComplete').addClass('hide');
    
    $(".addCompetenceBtn").click(function() {
        $('#addCompetenceContainer').removeClass('hide');
        $('#parentComplete').autocomplete({ source: getParents(), change: function (event, ui) { if(!ui.item){ $("#parentComplete").val(""); } } });
        }

);
    
    $('#addCompetenceForm').on('submit', function(e) {
        e.preventDefault();
        $('#addCompetenceContainer').addClass('hide');
        var name = $('input[name="name"]').val();
        var description = $('input[name="description"]').val();
        var parent = getParentId($('input[name="parent"]').val());
        addNewEvaluationElement(name, description, parent);
       location.reload();
        //TODO: dynamisch toevoegen inplaats van een reload function
    });
    
    $('#checkSubitem').change(function() {
        if($(this).is(":checked")) {
            $('#parentComplete').removeClass('hide');
        } else {
            $('#parentComplete').addClass('hide');
        }
    });
    
    $('#exportTable').on('click', function() {
        exportTableToExcel();
    });
}