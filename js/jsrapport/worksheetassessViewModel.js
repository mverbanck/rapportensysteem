/*
 * Module class
 */
function evaluatieElement(id, elementid, name, description, course, parent, number) {        //overkoepelende naam voor modules, doelstellingen, criteria
    return {
        id: ko.observable(id),
        elementid: ko.observable(elementid),
        name: ko.observable(name),
        description: ko.observable(description),
        course: ko.observable(course),
        parent: ko.observable(parent),
        children: ko.observableArray([]),
        number: ko.observable(number)
    };
}

// View model for the page
function pageViewModel(gvm) {
    // Page specific i18n bindings
    gvm.app  = ko.observable("Studenten beoordelen");
    gvm.title = ko.observable("Rapportensysteem Deeltijds Onderwijs");
    gvm.projectname = ko.observable("Rapportensysteem");
    
    gvm.userId = null;
    gvm.courseId = null;
    gvm.sheetId = null;
    gvm.worksheetfromstudentid = null;
    
    gvm.evaluatieElementen = ko.observableArray([]);
    gvm.assessMethod = ko.observableArray([]);
    
    gvm.clearStructure = function() {
        gvm.evaluatieElementen([]);
    }
    
    gvm.getAssessMethod = function(wid) {
        $.getJSON('/api/worksheetdata/' + wid, function(data) {
            switch(data[0].assessment) {
                case 'A - D':
                    var array = ['Geen', 'Afwezig', 'A', 'B', 'C', 'D'];
                    fillArray(array);
                    break;
                case '1 - 10':
                    var array = ['Geen', 'Afwezig', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
                    fillArray(array);
                    break;
                default:
                    //input field of slider?
                    break;
            }
        });
    }
}

function fetchCourseStructure() {
    viewModel.clearStructure();
    
    $.getJSON("/api/worksheetstructure/" + viewModel.sheetId, function(data){
        $.each(data, function(i, item){
            if (item.parent === null) {    //eerst alle hoofdelementen (null als parent) ophalen
                viewModel.evaluatieElementen.push(new evaluatieElement(item.id, item.elementid, item.name, item.description, item.course, item.parent, viewModel.evaluatieElementen().length + 1));
            };
        });
        insertLevelIntoStructure(viewModel.evaluatieElementen(), data, 2);
        manageEventHandlers(data);
    });
}

function insertLevelIntoStructure(collection, data, level) {
    if (level <= 5) {
        $.each(collection, function(i, element) {
            $.each(data, function(j, item) {    //per parent gaan kijken of hij children heeft
                if (item.parent === element.elementid()) {
                    var number = element.number() + '.' + parseInt(element.children().length + 1);
                    element.children.push(new evaluatieElement(item.id, item.elementid, item.name, item.description, item.course, item.parent, number));
                }
            });
            insertLevelIntoStructure(element.children(), data, level + 1);
        });
    }
}

function manageEventHandlers(data) {
    $.each(data, function(i, item) {
        $('#assessScore-' + item.id + ' li').click(function(e) {
            $(this).parent().parent().parent().find('.btn-assessScore span:first').text($(this).text());
            e.preventDefault();
        });
    });
}

function fillArray(array) {
    $.each(array, function(i, item) {
        viewModel.assessMethod.push({score: item});
    });
}
            
function getScores(collection, scores, level) {
    var score = "";
    if (level <= 5) {
        $.each(collection, function(i, element) {
            if($("#score-" + element.id()).length !== 0) {   //check if element with given id exists
                if ($('#score-' + element.id()).text() === "Geen") {
                    score = "";
                } else {
                    score = $('#score-' + element.id()).text();
                }
                scores.push({elementid: element.id(), score: score});
            }
            getScores(element.children(), scores, level + 1);
        });
    }
    return scores;
}

function addWorksheetScores(date, scores, worksheetPassed) {
    $.ajax({
        url: "/api/assessworksheet/" + viewModel.worksheetfromstudentid,
        type: "POST",
        data: {date: date, scores: scores, worksheetPassed: worksheetPassed},
        success: function(data) {
            console.log(data);
            window.location.replace('/worksheetrapporten');
        },
        error: function(data) {
            //callback(true, i18n.__('AssessGeneralError'));
        }
    });
}
    
function initPage() {        
    $.getJSON('/api/currentuser', function(data) {
        viewModel.userId = data.id;        
        viewModel.sheetUser = $('#storage').text();
        viewModel.sheetId = $('#header').attr('data-value');
        viewModel.worksheetfromstudentid = $('#storage2').attr('data-value');
        viewModel.courseId = $('#storage').attr('data-value');
        viewModel.getAssessMethod(viewModel.sheetId);
    
        fetchCourseStructure();
    });
    
    $('#submit').click(function() {
        var date = $('#date').val();
        var scores = getScores(viewModel.evaluatieElementen(), [], 1);
        var worksheetPassed = null;
        if ($('#worksheetPassed').prop('checked')) {
            worksheetPassed = 1;
        } else {
            worksheetPassed = 0;
        }
        
        if (date !== "") {
            addWorksheetScores(date, scores, worksheetPassed);
        } else {
            $('#errormessage').text('Werkfiche werd niet beoordeeld: geef een datum in!');
            $(document).scrollTop(0);
        }
    });
    
    $('#date').datepicker();
}
