var worksheets = [];
var worksheetsid = [];
var rowM = [];
var columnM = [];

//viewmodel for the assess page
function pageViewModel(gvm) {
    gvm.app  = ko.observable("Studenten beoordelen");
    gvm.title = ko.observable("Rapportensysteem Deeltijds Onderwijs");
    gvm.projectname = ko.observable("Rapportensysteem");

    gvm.userId = -1;
    gvm.availableCourses = ko.observableArray([]);
    gvm.availableStudents = ko.observableArray([]);

    gvm.currentCourseId = null;
    gvm.currentCourseName = null;
    gvm.currentStudentId = null;
    gvm.currentStudentName = null;

    gvm.cleanDate = null;
    gvm.evaluatieElementen = ko.observableArray([]);
    gvm.evaluatieElement = 0;

    gvm.scores = ko.observableArray([]);

    gvm.clearStructure = function() {
        gvm.evaluatieElementen([]);
    }

    gvm.addTableData = function(id, werkfiche, name, date, score) {
        if (score === "0" ) {
            score = 'Niet geslaagd';
        } else {
            score='geslaagd';
        }

        var tblOject = {id: id, twork: werkfiche, tname: name, tscore: score, tdatum: date};
        if (date !== null) {
            var dateArray = date.split("/");
            gvm.cleanDate = dateArray[0] + "-" + dateArray[1] + "-" + dateArray[2];
        } else {
            gvm.cleanDate = "";
        }

        gvm.tabledata.push(tblOject);

        $("#removebtn-" + id).click(function() {
            deleteTableItem(id, tblOject);
        });
    }

    gvm.clearTable = function() {
        gvm.tabledata.removeAll();
    }

    gvm.updateCourseRapport = function(currentstudentid) {
        $.getJSON('/api/getcoursesfromstudent/' + currentstudentid, function(data) {
            gvm.availableCourses.removeAll();
            $.each(data, function(i, item) {
                //  Put item in list
                gvm.availableCourses.push(item);
                // Add listener to listitem
                $("#coursebtn-" + item.id).click(function(){
                    gvm.currentCourseId = item.id;
                    $(".btn-courseRapport span:first").text($(this).text());
                    gvm.currentCourseName = $(this).text();
                    loadTablePage(viewModel.currentCourseId,  viewModel.currentStudentId);
                });
            });
        });
    };

    function getAllStudents() {
        studcombo = [];
        $.getJSON('/api/allstudents', function(data) {
            $.each(data, function(i, item) {
                studcombo.push({ label:item.firstname + " " + item.lastname, id: item.id });
            });
        });
        return studcombo;
    }

    //autocomplete
    $(function() {
        $( "#studentsComplete" ).autocomplete({
            delay: 0,
            source: getAllStudents(),
            select: function(event, ui) {
                gvm.currentStudentId = ui.item.id;
                gvm.currentStudentName = ui.item.label;
                gvm.updateCourseRapport(gvm.currentStudentId);
            }
        });
    });
    // The table data observable array
    gvm.tabledata = ko.observableArray([]);
    gvm.clearTable = function() {
        gvm.tabledata.removeAll();
    }
}

function deleteTableItem(id, tblOject) {
    showYesNoModal("Bent u zeker dat u dit item wil verwijderen? \r\n Let op: verwijderde items blijven in het systeem en kunnen weer actief gezet worden door een administrator. \r\n Gelieve de administrator te contacteren om een werkfiche definitief te verwijderen.", function(val){
        if(val){
            $.ajax({
                url: "/api/deleteassessedworksheet/" + id,
                type: "DELETE",
                success: function() {
                    viewModel.tabledata.remove(tblOject);
                }
            });
        }
    });
}

function loadTablePage(course, currentstud) {

    rowM = [];
    columnM = [];

    //Bovenste tabel + tabelkoppen 2e tabel.
    $.getJSON('/api/getworksheetsfromstudent/page/'+ viewModel.currentStudentId + '/' + course, function(data){
        // Clear current table page
        viewModel.clearTable();
        $(".table").find('tbody').empty();

        // Load table data
        $.each(data, function(i, item) {
            viewModel.addTableData(item.id, item.werkfiche, item.Name , item.datum, item.passed);
            rowM.push(item);
        });
        $(".table").trigger('update');
        $('[data-toggle="tooltip"]').tooltip();
        if(data.prev == "none"){
            $('#pager-prev-btn').addClass('disabled');
        } else {
            $('#pager-prev-btn').removeClass('disabled');
            $('#pager-prev-btn a').click(function(){
                loadTablePage(data.prev,viewModel.currentCourseId, viewModel.currentStudentId);
            });
        }
        if (data.next == "none"){
            $('#pager-next-btn').addClass('disabled');
        } else {
            $('#pager-next-btn').removeClass('disabled');
            $('#pager-next-btn a').click(function(){
                loadTablePage(data.next,viewModel.currentCourseId, viewModel.currentStudentId);
            });
        }
        // Number of pager buttons
        var numItems = $('.pager-nr-btn').length;
        // Calculate for the pager buttons
        var lowPage = Math.floor(pagenr/numItems) + 1;
        $('.pager-nr-btn').each(function() {
            // calculate current page number
            var thispagenr = lowPage++;
            // Add the page number
            $(this).html('<a href="#">' + thispagenr + '</a>');
            // Add active class to current page
            if(thispagenr == pagenr) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
            // Disable inactive classes and bind handlers to active classes
            if(thispagenr > data.pagecount) {
                $(this).addClass('disabled');
            } else {
                // Add click listener for button
                $(this).click(function() {
                    loadTablePage(thispagenr,viewModel.currentCourseId,  viewModel.currentStudentId);
                });
            }
        });
    });

    viewModel.clearStructure();
    //Ophalen evaluatie-elementen onderste tabel.
    $.getJSON("/api/coursestructure/" + course, function(data){
        var level = 1;
        $.each(data, function(i, item){
            if (item.parent === null) {    //eerst alle hoofdelementen (null als parent) ophalen
                viewModel.evaluatieElementen.push(new evaluatieElement(item.id, item.name, item.description, item.course, item.parent, level));
                rowM.forEach(function () {
                    columnM.push(item);
                })
                insertLevelIntoStructure(data, item, level, course, currentstud);
                level += 1;
            };
        });

        teller = 0;
        columnM.forEach(function() {
            //zorgt ervoor dat een row niet te lang wordt
            if (teller % rowM.length == rowM.length - 1) {
                var row='<td id=' + columnM[teller].id + '-' + rowM[teller % rowM.length].id + '>--</td>';
            } else {
                var row='<td id=' + columnM[teller].id + '-' + rowM[teller % rowM.length].id + '>--</td><td></td>';
            }
            $('tr#' + columnM[teller].id).append(row);
            teller ++;
        });

        //Ophalen punten van bepaalde werkfiche om later in de tabel te steken.
        $.getJSON('/api/getILTscore/' + course + '/' + currentstud, function(data) {
            $.each(data, function(i, item){
                $('#'+ item.elementid + '-' + item.werkfiche).html(item.score);
            });
        });
    });


}

function insertLevelIntoStructure(data, item, level, course, currentstud) {
    var levels = 1;
    $.each(data, function(i, element) {
        if (element.parent === item.id) {
            rowM.forEach(function () {
                columnM.push(element);
            })
            viewModel.evaluatieElementen.push(new evaluatieElement(element.id, element.name, element.description, element.course, element.parent, level + "." + levels));
            insertLevelIntoStructure(data,element, level + "." + levels, course);
            levels +=1;
        }
    });
}

/**
 * module class
 */
function evaluatieElement(id, name, description, course, parent, number) {        //overkoepelende naam voor modules, doelstellingen, criteria
    return {
        id: ko.observable(id),
        name: ko.observable(name),
        description: ko.observable(description),
        course: ko.observable(course),
        parent: ko.observable(parent),
        children: ko.observableArray([]),
        number: ko.observable(number),
        scores: ko.observableArray([])
    };
}

function initPage() {
    $.getJSON('/api/currentuser', function(data) {
        viewModel.userId = data.id;
    });
    //TODO table sorter weer uit commentaar hallen zodra tables af zijn.
    // $(".table").tablesorter();
}