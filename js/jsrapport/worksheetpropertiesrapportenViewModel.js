// View model for the page
function pageViewModel(gvm) {
    // Page specific i18n bindings
    gvm.app  = ko.observable("Studenten beoordelen");
    gvm.title = ko.observable("Rapportensysteem Deeltijds Onderwijs");
    gvm.projectname = ko.observable("Rapportensysteem");

    gvm.userId = null;

    gvm.evaluatieElementen = ko.observableArray([]);
    gvm.parents = ko.observableArray([]);     //wordt gebruikt voor de autocomplete
    gvm.evaluatieElement = 0;

    gvm.clearStructure = function() {
        gvm.evaluatieElementen([]);
        gvm.parents([]);
    }
}

function evaluatieElement(id, name, description, course, parent, number) {        //overkoepelende naam voor modules, doelstellingen, criteria
    return {
        id: ko.observable(id),
        name: ko.observable(name),
        description: ko.observable(description),
        course: ko.observable(course),
        parent: ko.observable(parent),
        children: ko.observableArray([]),
        number: ko.observable(number)
    };
}

function fetchCourseStructure() {
    viewModel.clearStructure();

    $.getJSON("/api/coursestructure/" + $('#storage').attr('data-value'), function(data){
        $.each(data, function(i, item){
            if (item.parent === null) {    //eerst alle hoofdelementen (null als parent) ophalen
                viewModel.evaluatieElementen.push(new evaluatieElement(item.id, item.name, item.description, item.course, item.parent, viewModel.evaluatieElementen().length + 1));
                viewModel.parents.push({id: item.id, name: item.name, number: viewModel.evaluatieElementen().length + 1});
            };
        });
        insertLevelIntoStructure(viewModel.evaluatieElementen(), data, 2);
        manageEventHandlers(data);
    });
}

function insertLevelIntoStructure(collection, data, level) {
    if (level <= 5) {
        $.each(collection, function(i, element) {
            $.each(data, function(j, item) {    //per hoofdparent gaan kijken of hij children heeft
                if (item.parent === element.id()) {
                    var number = element.number() + '.' + parseInt(element.children().length + 1);
                    element.children.push(new evaluatieElement(item.id, item.name, item.description, item.course, item.parent,number));
                    viewModel.parents.push({id: item.id, name: item.name, number: number});
                }
            });
            insertLevelIntoStructure(element.children(), data, level + 1);
        });
    }
}

function manageEventHandlers(data) {
    $.each(data, function(i, item) {
        $('#checkbox-' + item.id).on('click', function() {
            //als checkbox checked
            if ($('#checkbox-' + item.id).prop('checked')) {
                var temp = item;
                while(temp.parent !== null) {
                    $('#checkbox-' + temp.parent).prop('checked', true);
                    $.each(data, function(i, items) {
                        if (items.id == temp.parent) {
                            temp = items;
                        }
                    });
                }
            }
            else {
                $.each(data, function(i, items) {
                    //Als geklikt item parent is van het afgelopen item check dan de checkbox hiervan.
                    if (item.id == items.parent) {
                        if ($('#checkbox-' + items.id).prop('checked')) {
                            $('#checkbox-' + items.id).trigger("click");
                        }
                    }
                });
            }
        });
    });
}

function getAllCheckedFiels(){
    var modulesChecked=[];
    $("input:checkbox").each(function(){
        var $this = $(this);
        if($this.is(":checked")){
            temp = $this.attr("id").split("-");
            modulesChecked.push(temp[1]);
        }
    });
    return modulesChecked;
}

function addWorksheetProperties(serialData, wid, collection, assessMethod,closed, callback) {

    //properties ajax post
    $.ajax({
        url: "/api/worksheetproperties/" + wid + "/" + assessMethod + "/" + closed,
        type: "PUT",
        data: serialData,
        success: function(data) {
            console.log(data);
        },
        error: function(data) {
            callback(true, i18n.__('AssessGeneralError'));
        }
    });
    //modules ajax post
    $.ajax({
        url: "/api/worksheetmodules",
        type: "POST",
        data: {id: wid, modules: collection},
        success: function(data) {
            window.location.replace('/worksheetrapporten');
        },
        error: function(data) {
            callback(true, i18n.__('AssessGeneralError'));
        }
    });
}

function getWorksheetProperties(wid) {
    //Algemene info ophalen en invullen
    $.getJSON('/api/worksheetdata/' + wid, function(data) {
        $('#equipment').val(data[0].equipment);
        $('#method').val(data[0].method);
        $(".btn-assessMethod span:first").text(data[0].assessment);
    });

    //Ophalen welke elementen al eerder aangeduid waren
    $.getJSON('/api/worksheetstructure/' + wid, function(data) {
        $.each(data, function(i, item){
            $('#checkbox-' + item.elementid).prop('checked', true);
        });
    });
}

function removeOldElements(wid) {
    console.log(wid);
    $.ajax({
        url: "/api/werficheElementsDelete/" + wid,
        type: "DELETE",
        success: function() {
            console.log("oude elementen gewist");
        }
    });
}

function initPage() {
    //event handlers for dropdown items
    $('ul.dropdown-assessMethod li').click(function() {
        $(".btn-assessMethod span:first").text($(this).text());
    });

    fetchCourseStructure();

    $('#submitdef').click(function() {
        //Alle aangeduide competenties opslaan in Array
        modulesChecked = getAllCheckedFiels();

        removeOldElements($('#header').attr('data-value'));
        var closed = 1;
        addWorksheetProperties($('#worksheetform').serialize(), $('#header').attr('data-value'), modulesChecked, $('.btn-assessMethod span:first').text(),closed, function(result, message) {
            $('#errormessage').text(message);
        });
    });

    $('#submittemp').click(function() {
        //Alle aangeduide competenties opslaan in Array
        modulesChecked = getAllCheckedFiels();

        removeOldElements($('#header').attr('data-value'));
        var closed = 0;
        addWorksheetProperties($('#worksheetform').serialize(), $('#header').attr('data-value'), modulesChecked, $('.btn-assessMethod span:first').text(), closed, function(result, message) {
            $('#errormessage').text(message);
        });
    });

    //Achterhalen of we een nieuwe werkfiche aan het aanmaken zijn of een oude gaan bijwerken
    var res = window.location.pathname.split("/");
    if(res[6] == 1) {
        console.log("bijwerken oude fiche");
        getWorksheetProperties($('#header').attr('data-value'));
    }

    // Feedback Form

    // Feedback Form
}