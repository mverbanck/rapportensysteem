var students = [];
var studentids = [];

// View model for the page
function pageViewModel(gvm) {
    // Page specific i18n bindings
    gvm.app  = ko.observable("Werkfiches");
    gvm.title = ko.observable("Rapportensysteem Deeltijds Onderwijs");
    gvm.projectname = ko.observable("Rapportensysteem");
    
    gvm.availableCourses = ko.observableArray([]);
    gvm.allCourses = ko.observableArray([]);
    gvm.tabledata = ko.observableArray([]);   
    gvm.currentCourseId = null;
    gvm.assessWorksheetId = null;
    gvm.assessWorksheetName = null;

    gvm.newWorksheet = null;

    gvm.updateCourseRapport = function() {
        $.getJSON('/api/getcoursesfromteacher/' + gvm.userId, function(data) {
            gvm.availableCourses.removeAll();
            $.each(data, function(i, item) {
                //  Put item in list
                gvm.availableCourses.push(item);

                // Add listener to listitem
                $("#coursebtn-" + item.id).click(function(){
                    gvm.currentCourseId = item.id;
                    $(".btn-courseRapport span:first").text($(this).text());
                    gvm.saveLastSelectedDropdowns();
                    loadTablePage(1);
                });
            });
        });
    };

    gvm.saveLastSelectedDropdowns = function() {
        $.ajax({
            url: "/api/saveDropDownCourse/" + gvm.userId + "/" + $(".btn-courseRapport span:first").text() + "/" + viewModel.currentCourseId,
            type: "PUT",
            success: function(data) {
                console.log("Dropdown opslaan is gelukt.")
            },
            error: function(data) {
                console.log("Dropdown is niet opgeslagen.")
            }
        });
    }

    gvm.addTableData = function(id, name, closed) {
        // Push data
        var tblOject = {tid: id, tname: name, tclosed: closed};
        gvm.tabledata.push(tblOject);

        $('#removebtn-' + id).bind('click', function(event, data){
            deleteTableItem(id, tblOject);
            event.stopPropagation();
        });
        $('#editbtn-' + id).bind('click', function(event, data){
            showEditWorksheetModal(id, name);
            event.stopPropagation();
        });
        $('#assessbtn-' + id).bind('click', function(event, data){
            gvm.assessWorksheetId = id;
            gvm.assessWorksheetName = name;
            $("#assessStudentForm").show();
            $('#studentComplete').autocomplete({ source: getAllStudents() });
            event.stopPropagation();
            $('html, body').animate({
                scrollTop: $("#assessStudentForm").offset().top
            }, 1000);
        });

        $('#copybtn-' + id).bind('click', function(event){
            viewModel.assessWorksheetId = id;
            viewModel.assessWorksheetName = name;
            showCopySheetToCourseModal();
            getAllCourses();
        });

        //Zorgt ervoor dat gesloten werkfiches niet gewijzigd kunnen worden maar wel gequateerd
        if (closed === '1') {
            $('#editbtn-' + id).hide();
        } else {
            $('#assessbtn-' + id).hide();
        }
    }
    
    gvm.clearTable = function() {
        gvm.tabledata.removeAll();
    }
}

function getAllStudents() {
    students = [];
    studentids = [];
    $.getJSON('/api/getstudentsfromcourse/' + viewModel.currentCourseId, function(data) {
        $.each(data, function(i, item) {
            students.push(item.firstname + ' ' + item.lastname);
            studentids.push(item.id);
        });
    });
    return students;
}

function getStudentId() {
    var i = 0;
    var student = 0;
    students.forEach(function(entry) {
        if (new String(entry).valueOf() == new String($('#studentComplete').val()).valueOf()) {
            student = studentids[i];
        }
        i+= 1;
    });
    return student;
}

function getAllCourses() {
    allCourses = [];
    
    $.getJSON('/api/coursedrop', function(data) {
        $.each(data, function(i, item) {
            allCourses.push(item.name);
            viewModel.allCourses.push({ id: item.id, name: item.name });
        });
        $('#courseComplete').autocomplete({ source: allCourses, change: function (event, ui) { if(!ui.item){ $("#courseComplete").val(""); } } });
    }); 
}

function getCourseIdWithName(coursename) {
    var courseid = null;
    $.each(viewModel.allCourses(), function(i, item) {
        if (item.name === coursename) {
            courseid = item.id;
        }
    });
    return courseid;
}

function addNewWorksheet(serialData, courseid, callback) {
    $.ajax({
        url: "/api/addworksheet/" + courseid,
        type: "POST",
        data: serialData,
        success: function(data) {
            callback(true);
            window.location = "/api/worksheet/" + data['id'] + "/" + data['name'] + '/' + courseid+ '/' + 0;
        },
        error: function(data) {
            callback(false);
        }
    });
}

function updateWorksheet(id, serialData, courseid, callback) {
    nieuw = (serialData.split("="));
    $.ajax({
        url: "/api/updateWorksheet/" + id + "/" + nieuw[1],
        type: "PUT",
        success: function(data) {
            window.location = "/api/worksheet/" + id + "/" + nieuw[1] + '/' + courseid + '/' + 1;
            console.log("Vak wijzigen geslaagd.")
        },
        error: function(data) {
            console.log("vak wijzigen is niet gelukt.")
        }
    });
}

function copyWorksheet(courseid) {
    //Aanmaken werkfiche zonder bijhorende elementen
    $.ajax({
        url: "/api/addworksheetNoMeta/" + viewModel.assessWorksheetName + "_Kopie" + "/" + courseid,
        type: "POST",
        success: function(data) {
            console.log("Oud id: " + viewModel.assessWorksheetId);
            console.log("Nieuw id: " + data.id);
            viewModel.newWorksheet = data.id;
        },
        error: function(data) {
            console.log("Copy failed");
        }
    });

    //Algemene info ophalen & wegschrijven
    $.getJSON('/api/worksheetdata/' + viewModel.assessWorksheetId, function(data) {
        $.ajax({
            url: "/api/worksheetpropertiesNoMeta/" + viewModel.newWorksheet + "/" + data[0].equipment +  "/" + data[0].method + "/" + data[0].assessment + "/" + 0,
            type: "PUT",
            success: function(data) {
                console.log(data);
            },
            error: function(data) {
                console.log("properties wegschrijven is gefaald.");
            }
        });
    });

    var elements = [];
    //elementen ophalen
    $.getJSON('/api/worksheetstructure/' + viewModel.assessWorksheetId, function(data) {
        $.each(data, function(i, item){
            console.log("item toevoegen");
            elements.push(item.elementid);
        });

        //elementen opslaan
        $.ajax({
            url: "/api/worksheetmodules",
            type: "POST",
            data: {id: viewModel.newWorksheet, modules: elements},
            success: function(data) {
                loadTablePage(1);
            },
            error: function(data) {
                console.log("evaluatie elementen opslaan is niet gelukt")
            }
        });
    });
}

function deleteTableItem(id, tblOject) {
    showYesNoModal("Bent u zeker dat u dit item wil verwijderen? \r\n Let op: verwijderde items blijven in het systeem en kunnen weer actief gezet worden door een administrator. \r\n Gelieve de administrator te contacteren om een vak definitief te verwijderen.", function(val){
        if(val){
            $.ajax({
                url: "/api/deleteworksheet/" + id,
                type: "DELETE",
                success: function() {
                    viewModel.tabledata.remove(tblOject);
                }
            });
        }
    });
}

function showNewWorksheetModal() {
    resetGeneralModal();
    setGeneralModalTitle(i18n.__("AddNewWorksheet"));
    setGeneralModalBody('<form id="newworksheetform"> \
            <div class="form-group"> \
                <input type="text" class="form-control input-lg" placeholder="' + i18n.__('NameTableTitle') + '" " name="name"> \
            </div> \
            </form>' );

    addGeneralModalButton(i18n.__("AddBtn"), function(){
       addNewWorksheet($('#newworksheetform').serialize(), viewModel.currentCourseId, function(result){
            hideModal();
        });
    });

    showGeneralModal();
}

function showEditWorksheetModal(id, name)
{
    resetGeneralModal();
    setGeneralModalTitle(i18n.__("EditWorksheet"));
    setGeneralModalBody('<form id="updateworksheetform"> \
            <div class="form-group"> \
                <input type="text" class="form-control input-lg" placeholder="' + i18n.__('NameTableTitle') + '" " name="name" value="' + name + '"> \
            </div> \
        </form>');
    $.getJSON()

    addGeneralModalButton(i18n.__("SaveBtn"), function(){
        updateWorksheet(id, $('#updateworksheetform').serialize(), viewModel.currentCourseId, function(result){
            hideModal();
        });
    });

    addGeneralModalButton(i18n.__("CancelBtn"), function(){
        hideModal();
    })

    showGeneralModal();
}

function giveWorksheetToStudent(wid, studentid, studentname) {
    $.ajax({
        url: "/api/addworksheetstudent",
        type: "POST",
        data: {worksheetid: wid, studid: studentid},
        success: function(data) {
            console.log('succes');
            window.location = '/api/worksheetassess/' + viewModel.assessWorksheetId + '/' + viewModel.assessWorksheetName + '/' + viewModel.currentCourseId + '/' + studentid + '/' + studentname + '/' + data['id'];
        },
        error: function(data) {
            console.log('failure');
        }
    });
}

function loadLastSelectedDropdowns(user) {
    $.getJSON('/api/lastdropdownrapporten/' + user, function (data) {
        viewModel.currentCourseId = data[0].courseid;
        loadTablePage(1);
        $(".btn-courseRapport span:first").text(data[0].course);
    });
}

function loadTablePage(pagenr)
{
    $.getJSON('/api/worksheets/page/' + pagenr + '/' + viewModel.currentCourseId, function(data){

        /* Clear current table page */
        viewModel.clearTable();
        $(".table").find('tbody').empty();

        // Load table data
        $.each(data.data, function(i, item) {
            viewModel.addTableData(item.id, item.Name, item.closed);
        });
        $('[data-toggle="tooltip"]').tooltip();
        $(".table").trigger('update');

        //$(".table").tablesorter();

        /* Let previous en next buttons work */
        if(data.prev == "none"){
            $('#pager-prev-btn').addClass('disabled');
        } else {
            $('#pager-prev-btn').removeClass('disabled');
            $('#pager-prev-btn a').click(function(){
                loadTablePage(data.prev);
            });
        }

        if(data.next == "none"){
            $('#pager-next-btn').addClass('disabled');
        } else {
            $('#pager-next-btn').removeClass('disabled');
            $('#pager-next-btn a').click(function(){
                loadTablePage(data.next);
            });
        }

        // Number of pager buttons
        var numItems = $('.pager-nr-btn').length;

        /* Calculate for the pager buttons */
        var lowPage = Math.floor(pagenr/numItems) + 1;

        $('.pager-nr-btn').each(function() {
            /* calculate current page number */
            var thispagenr = lowPage++;

            /* Add the page number */
            $(this).html('<a href="#">' + thispagenr + '</a>');

            /* Add active class to current page */
            if(thispagenr == pagenr) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }

            /* Disable inactive classes and bind handlers to active classes */
            if(thispagenr > data.pagecount) {
                $(this).addClass('disabled');
            } else {
                /* Add click listener for button */
                $(this).click(function() {
                    loadTablePage(thispagenr);
                });
            }
        });
        });
}

function initPage() {
    $("#errormessage").hide();

    $('#addWorksheetBtn').click(function() {
        if (viewModel.currentCourseId === undefined || viewModel.currentCourseId === null) {
            $("#errormessage").show();
        } else {
            showNewWorksheetModal();
            $("#errormessage").hide();
        }
    });
    
    $('#copySheetToCourse').on('click', function() {
        var courseid = getCourseIdWithName($('#courseComplete').val());
        copyWorksheet(courseid);
        hideModal();
    });
    
    $.getJSON('/api/currentuser', function(data) {
        viewModel.userId = data.id;
        viewModel.updateCourseRapport();
        loadLastSelectedDropdowns(data.id);

    });

    $('#assessStudentForm').hide();

    $('#assessStudentBtn').click(function() {
        var studentid = getStudentId();
        var studentname = $('#studentComplete').val();
        giveWorksheetToStudent(viewModel.assessWorksheetId, studentid, studentname);

        //Indien gewenst beoordeelformulier weer verbergen.
        $('#studentComplete').val("");
        $('#assessStudentForm').hide();
    });

    $(".table").tablesorter();
}

