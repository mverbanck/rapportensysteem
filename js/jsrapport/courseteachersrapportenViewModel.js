var userid;
var allTeachers = [];
var courseTeachers = [];
var availableTeachers = [];

function pageViewModel(gvm) {
    gvm.title = ko.observable("Rapportensysteem Deeltijds Onderwijs");
    gvm.projectname = ko.observable("Rapportensysteem");
    
    //gvm.projecttitle = ko.observable("");
    gvm.userId = -1;
    gvm.coupledCount = 0;

    // The table data observable array
    gvm.tabledata = ko.observableArray([]);

    // Add data to the table
    gvm.addTableData = function(userid, name) {
        // Push data
        var tblOject = {tid: userid, tteacher: name};
        gvm.tabledata.push(tblOject);

        // Attach delete handler to delete button
        $('#removebtn-' + userid).bind('click', function(event, data){
            // Delete the table item
            deleteTableItem(userid, tblOject);
            event.stopPropagation();
        });
            }

    gvm.clearTable = function() {
        gvm.tabledata.removeAll();
    }

 }

/*
 * Load page of the table
 */
function loadTablePage(pagenr,course)
{
    $.getJSON('/api/getTeachersByCourseId/page/' + pagenr + '/' + course, function(data){

        /* Clear current table page */
        viewModel.clearTable();

        // Load table data
        $.each(data.data, function(i, item) {

           viewModel.addTableData(item.userid, item.firstname + " " + item.lastname);


        });
        $('[data-toggle="tooltip"]').tooltip();
        $(".table").tablesorter();
        //TODO pagers doen werken
        //Momenteel wordt enkel alles geselecteerd met LIMIT 0,20
        //Maar de pagers zelf blijven dissabled waardoor het ook niet mogelijk is LIMIT 21,40 op te vragen.
        /* Let previous en next buttons work */


        if(data.prev == "none"){
            $('#pager-prev-btn').addClass('disabled');
        } else {
            $('#pager-prev-btn').removeClass('disabled');
            $('#pager-prev-btn a').click(function(){
                loadTablePage(data.prev);
            });
        }

        if (data.next == "none"){
            $('#pager-next-btn').addClass('disabled');
        } else {
            $('#pager-next-btn').removeClass('disabled');
            $('#pager-next-btn a').click(function(){
                loadTablePage(data.next);
            });
        }

        // Number of pager buttons
        var numItems = $('.pager-nr-btn').length;


        /* Calculate for the pager buttons */
        var lowPage = Math.floor(pagenr/numItems) + 1;


        $('.pager-nr-btn').each(function() {
            /* calculate current page number */
            var thispagenr = lowPage++;

            /* Add the page number */
            $(this).html('<a href="#">' + thispagenr + '</a>');

            /* Add active class to current page */
            if(thispagenr == pagenr) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }

            /* Disable inactive classes and bind handlers to active classes */
            if(thispagenr > data.pagecount) {
                $(this).addClass('disabled');
            } else {
                /* Add click listener for button */
                $(this).click(function() {
                    loadTablePage(thispagenr, course);
                });
            }
        });
    });
}

function getAllTeachers() {
    allTeachers = [];
    
    $.getJSON('/api/getAllTeachers', function(data) {
        $.each(data, function(i, item) {
            allTeachers.push({tid: item.id, tname: item.firstname + " " + item.lastname});
        });
        getCourseTeachers(allTeachers);
    }); 
}

function getCourseTeachers(allTeachers) {
    courseTeachers = [];
    
    $.getJSON('/api/getCourseTeachers/' + $('#projectHeader').attr("data-value"), function(data) {
        $.each(data, function(i, item) {
            courseTeachers.push(item.teacher);
        });
        getAvailableTeachers(allTeachers, courseTeachers);
    });
}

function getAvailableTeachers(allTeachers, courseTeachers) {
    availableTeachers = [];
    
    $.each(allTeachers, function(i, item) {
        if (courseTeachers.indexOf(item.tid) < 0) {
            availableTeachers.push(item.tname);
        }
    });
    
    $('#teacherComplete').autocomplete({ source: availableTeachers });
}

function getTeacherIdWithName(teacherName) {
    var tid = null;
    $.each(allTeachers, function(i, item) {
        if (item.tname === teacherName) {
            tid = item.tid;
        }
    });
    return tid;
}

/*
 * Delete item from table given the id.
 */
function deleteTableItem(id, tblOject) {
    showYesNoModal("Bent u zeker dat u deze leerkracht wilt loskoppelen van dit vak?", function(val){
        if(val){
             $.ajax({
                url: "/api/deleteTeacherFromCourse/" + $('#projectHeader').attr("data-value") + "/" + id,
                type: "DELETE",
                success: function() {
                   viewModel.tabledata.remove(tblOject);
                }
             });

        }
    });
}

function addTableItem(courseid, teacherid, callback) {
    $.ajax({
        url: "/api/addteachertocourse/" + courseid + "/" + teacherid,
        type: "POST",
        success: function(data) {
            //viewModel.addTableData(data['id'], data['code'], data['name'], data['description']);
            callback(true);
        },
        error: function(data) {
            callback(false);
        }
    });
} 

function initPage() {
    $('#addCourseTeachers').click(function() {
        showTeacherModal();
        getAllTeachers();
    });
    
    $('#addTeacher').on('click', function() {
        var teacherid = getTeacherIdWithName($('#teacherComplete').val());
        addTableItem($('#projectHeader').attr("data-value"), teacherid , function(result){
            hideModal();
        });
    });
    
    $.getJSON('/api/currentuser', function(data) {
        viewModel.userId = data.id;
        userid = data.id;
    });
    loadTablePage(1,$('#projectHeader').attr("data-value"));
    
    $('#goBackBtn').on('click', function() {
        window.location.replace('/coursesrapporten');
    });
    
    // Feedback Form
    

    
    // Feedback Form
}
