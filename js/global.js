// Page wide variables  
var viewModel = null;

// Instantiate localisation 
var i18n = new I18n({
    directory: "/locales",
    locale: document.documentElement.lang,
    extension: ".json"
});

/**
 * Change the UI locale
 * @param {string} locale - the new locale in two letters
 * @returns {boolean} returns false to prevent event propagation
 */
function setLang(locale) {
    viewModel.setLocale(locale);
    
    // Close navbar when open
    $(".navbar-collapse").stop().css({ 'height': '1px' }).removeClass('in').addClass("collapse");
    $(".navbar-toggle").stop().removeClass('collapsed');
    return false;
}

/**
 * The system global viewmodel
 * @returns {GlobalViewModel}
 */
function GlobalViewModel()
{
    // Application settings
    this.lang = ko.observable("en");
    this.app = ko.observable("Rapportensysteem");
        
    // I18N bindings
    this.loginModalTitle = ko.observable("Aanmelden");
    this.homeBtn = ko.observable("Home");
    this.assessBtn = ko.observable("Beoordeel");
    this.structureBtn = ko.observable("Structuur");
    this.forgotPswdBtn = ko.observable("Wachtwoord vergeten?");
    this.notMemberBtn = ko.observable("Nog geen lid?");
    this.loginBtn = ko.observable("Aanmelden");
    this.logoutBtn = ko.observable("Afmelden");
    this.email = ko.observable("Email");
    this.password = ko.observable("Wachtwoord");
    this.yesNoModaltitle = ko.observable("Bent u zeker?");
    this.yesNoModalBody = ko.observable("Bent u zeker dat u dit item wilt verwijderen?");
    this.yes = ko.observable("Ja"); 
    this.no = ko.observable("Nee");
    this.uploadModalTitle = ko.observable("Upload een bestand");
    this.upload = ko.observable("Bestand uploaden");
    this.chooseFiles = ko.observable("Kies bestanden");
    this.uploadedFiles = ko.observable("Geslaagde uploads");
    this.progress = ko.observable("Voortgang");


    /**
     * Change the UI locale
     * @param {string} locale - the new UI locale
     */
    this.setLocale = function(locale) {
        this.lang(locale);
        i18n.setLocale(this.lang());
    };
    
    // Instantiate page viewmodel
    pageViewModel(this);
}

/**
 * Page initialisation function
 */
$('document').ready(function(){
    // Activate knockout framework
    viewModel = new GlobalViewModel();
    ko.applyBindings(viewModel, document.getElementById("htmldoc"));

    // Execute page specific initialisation if present
    if(typeof initPage == 'function'){
        initPage();
    }
});

/*
 * Global extensions
 */
if (typeof String.prototype.startsWith != 'function') {
  // see below for better implementation!
  String.prototype.startsWith = function (str){
    return this.indexOf(str) == 0;
  };
}